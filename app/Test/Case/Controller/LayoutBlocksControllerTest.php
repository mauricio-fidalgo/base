<?php
App::uses('LayoutBlocksController', 'Controller');

/**
 * LayoutBlocksController Test Case
 *
 */
class LayoutBlocksControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.layout_block',
		'app.layout_section',
		'app.menu_page',
		'app.comment_block',
		'app.comments',
		'app.option',
		'app.user',
		'app.group'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testUserIndex method
 *
 * @return void
 */
	public function testUserIndex() {
	}

/**
 * testUserView method
 *
 * @return void
 */
	public function testUserView() {
	}

/**
 * testUserAdd method
 *
 * @return void
 */
	public function testUserAdd() {
	}

/**
 * testUserEdit method
 *
 * @return void
 */
	public function testUserEdit() {
	}

/**
 * testUserDelete method
 *
 * @return void
 */
	public function testUserDelete() {
	}

}
