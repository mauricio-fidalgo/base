<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ClearComponent', 'Controller/Component');

/**
 * ClearComponent Test Case
 *
 */
class ClearComponentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		''
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$Collection = new ComponentCollection();
		$this->Clear = new ClearComponent($Collection);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Clear);

		parent::tearDown();
	}

/**
 * testBeforeFilter method
 *
 * @return void
 */
	public function testBeforeFilter() {
	}

/**
 * testLogin method
 *
 * @return void
 */
	public function testLogin() {
	}

/**
 * testLogout method
 *
 * @return void
 */
	public function testLogout() {
	}

/**
 * testAuthLogin method
 *
 * @return void
 */
	public function testAuthLogin() {
	}

/**
 * testShowInfo method
 *
 * @return void
 */
	public function testShowInfo() {
	}

/**
 * testBeginSession method
 *
 * @return void
 */
	public function testBeginSession() {
	}

/**
 * testEndSession method
 *
 * @return void
 */
	public function testEndSession() {
	}

/**
 * testHasTimedOut method
 *
 * @return void
 */
	public function testHasTimedOut() {
	}

/**
 * testSetTimeStamp method
 *
 * @return void
 */
	public function testSetTimeStamp() {
	}

/**
 * testGetGroupRules method
 *
 * @return void
 */
	public function testGetGroupRules() {
	}

/**
 * testIsLogged method
 *
 * @return void
 */
	public function testIsLogged() {
	}

/**
 * testGetUserId method
 *
 * @return void
 */
	public function testGetUserId() {
	}

/**
 * testIsAuthorized method
 *
 * @return void
 */
	public function testIsAuthorized() {
	}

/**
 * testGetLoggedUser method
 *
 * @return void
 */
	public function testGetLoggedUser() {
	}

}
