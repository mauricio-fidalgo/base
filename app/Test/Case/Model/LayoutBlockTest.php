<?php
App::uses('LayoutBlock', 'Model');

/**
 * LayoutBlock Test Case
 *
 */
class LayoutBlockTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.layout_block',
		'app.layout_section',
		'app.menu_page'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LayoutBlock = ClassRegistry::init('LayoutBlock');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LayoutBlock);

		parent::tearDown();
	}

}
