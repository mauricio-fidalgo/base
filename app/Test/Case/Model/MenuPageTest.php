<?php
App::uses('MenuPage', 'Model');

/**
 * MenuPage Test Case
 *
 */
class MenuPageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.menu_page',
		'app.layout_block',
		'app.layout_section'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MenuPage = ClassRegistry::init('MenuPage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MenuPage);

		parent::tearDown();
	}

}
