<?php
App::uses('LayoutSection', 'Model');

/**
 * LayoutSection Test Case
 *
 */
class LayoutSectionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.layout_section',
		'app.layout_block',
		'app.menu_page'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LayoutSection = ClassRegistry::init('LayoutSection');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LayoutSection);

		parent::tearDown();
	}

}
