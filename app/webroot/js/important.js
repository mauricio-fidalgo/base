$(window).load(function(){
    $("input[type=checkbox],input[type=radio],select").uniform();
    if($(document).height()>$(".vertical-aligned").height()){
        $(".vertical-aligned").css({'position':'absolute',
                                'top':$(document).height()/2-$(".vertical-aligned").height(),
                                'width':$(".vertical-aligned").parent().width()});
    }
    $("showready").css("display","block");
});
$(document).ready(function(){
    $("#flashMessage").click(function(){
        var $this = $(this);
        $this.slideUp("fast",function(){$this.remove()});
    });
    $(window).resize(function(){
        if($(document).height()>$(".vertical-aligned").height() && 0<($(window).height()/2-$(".vertical-aligned").height())){
            $(".vertical-aligned").css({'position':'absolute',
                                        'top':$(window).height()/2-$(".vertical-aligned").height(),
                                        'width':$(".vertical-aligned").parent().width()});
        }else{
            $(".vertical-aligned").css({'position':'none',
                                        'top':0});
        }
    });
    $("[rel^=prettyPhoto]").prettyPhoto();
})