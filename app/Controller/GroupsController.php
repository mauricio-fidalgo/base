<?php

App::uses("AppController","Controller");
class GroupsController extends AppController
{
    public $components = array('Utils','Session');
    
    /**
     * 
     */
    public function user_index(){
        $data = $this->paginate("Group");
        $this->set("data",$data);
    }
    
    /**
     * 
     */
    public function user_view($id = null){
        $group = $this->Utils->verifyEntry($id,"Group");
        $this->set("data",$group);
    }
     
    /**
     * 
     */
    public function user_add(){
        $this->set("controllers",$this->Utils->getAllControllers());
        if ($this->request->is("post") || $this->request->is("put")) {
            $data = $this->request->data;
            if($this->Group->save($data))
                $this->Session->setFlash("Grupo criado com sucesso!");
            else
                $this->Session->setFlash("O grupo não pôde ser editado, "
                                       . "por favor tente novamente.");
        }
    }
    
    /**
     * 
     */
    public function user_edit($id = null){
        $this->set("Controllers",$this->Utils->getAllControllers());
        $this->Utils->recursive = 0;
        $group = $this->Utils->verifyEntry($id,"Group");
        if($this->request->is("post")){
            $data = $this->request->data;
			$this->Group->id = $id;
            if($this->Group->save($data))
                $this->Session->setFlash("Grupo Editado com sucesso!");
            else
                $this->Session->setFlash("O grupo não pôde ser editado, "
                                       . "tente novamente.");
        }
        if(!$this->request->data)
            $this->request->data = $group;
    }
    
    /**
     * 
     */
    public function user_delete($id){
        if(!$this->request->is("post"))
            throw new MethodNotAllowedException();
        if($this->Utils->verifyEntry($id,"Group",false)){
            if($this->Group->delete($id))
                $this->Session->setFlash("Grupo Apagado com sucesso!");
            else
                $this->Session->setFlash("O grupo não pôde ser apagado, "
                                       . "tente novamente.");
            $this->Utils->redirectBack();
        }
    }
}
