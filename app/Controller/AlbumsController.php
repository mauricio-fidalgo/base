<?php
App::uses('AppController', 'Controller');
class AlbumsController extends AppController {
	public $uses = array("Photo","Album","AlbumGroup");
	public $components = array('Session');
	public $helpers = array("Tinymce");
	public function user_index(){
		$this->paginate = array(
			'Album' => array(
				'fields' => array('Album.id','Album.name','Album.subtitle','Album.slide','Album.erasable','Album.created'),
				'limit' => 10,
				'order' => array('Album.created' => 'desc'),
			),
		);
		$paginate = $this->paginate("Album",$recursive = 0);
		foreach($paginate as &$albums){
			$x = 0;
			foreach($albums['Photo'] as &$photo){
				$x++;
			}
			$albums['Album']['qtd'] = $x;
		}
		$this->set("albums",$paginate);
	}
	public function user_view($id = null){
		$this->Album->id = $id;
		$this->set("prettyPhoto",true);
		if ($this->Album->exists())
			$this->set("album",$this->Album->read());
		else
			throw new Exception("Album inexistente");
	}
	public function user_edit($id = null){
		$this->Album->id = $id;
		$album = $this->Utils->verifyEntry($id, "Album");
		$this->set("album",$album);
		if($this->request->is("post") || $this->request->is("put")){
			$data = $this->request->data;
			if($this->Album->save($data)){
				$this->Session->setFlash('O Album foi editado com sucesso.','default', array('class'=>'message success'));
				$this->redirect(array("action"=>"user_index"));
			}else{
				$this->Session->setFlash('O Album não pode ser editado.','default', array('class'=>'message error'));
			}
		}else
			$this->request->data = $this->Album->read();
	}
	public function user_add($id = null){
		if ($this->request->is('Post')){
			$data = $this->request->data;
			if($this->Album->save($data)){
				$this->Session->setFlash('O Album foi criado com sucesso.','default', array('class'=>'message success'));
				$this->redirect(array("action"=>"user_index"));
			}else{
				$this->Session->setFlash('O Album não pôde ser criado. Tente novamente.','default', array('class'=>'message error'));
			}
		}
	}
	public function user_delete($id = null){
		if (!$this->request->is('post') && !$this->request->is('put')) {
			throw new MethodNotAllowedException();
		}
		$this->Album->id = $id;
		if (!$this->Album->exists()){
			throw new Exception("Album inexistente");
		}
		$album = $this->Album->read("Album.erasable");
		if ($album['Album']['erasable'] == 0){
			throw new Exception("Este album não pode ser apagado");
		}
		unset($album);
		if($this->Album->delete($id, true)){
			$this->Session->setFlash('O Album foi apagado com sucesso.','default', array('class'=>'message success'));
			$this->redirect(array('action'=>'user_index'));
		} else {
			$this->Session->setFlash('O Album não pode ser apagado.','default', array('class'=>'message error'));
		}
	}
	public function ajax_listPhotos($id = null){
		if($this->request->is("ajax")){
			if($id == null){
				$albumList = $this->Album->find('all',
					array(
						'fields' => array('Album.id', 'Album.name')
					)
				);
				$this->set("albumList",json_encode($albumList));
			} else {
				$album = $this->Utils->verifyEntry($id,"Album");
				$this->set("album", $album);
			}
		} else{
			throw new MethodNotAllowedException;
		}
	}
}