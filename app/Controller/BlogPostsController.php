<?php
App::uses('AppController', 'Controller');
class BlogPostsController extends AppController{
	public $uses = array('BlogPost');
	public $helpers = array('Tinymce');
	public function user_view($id = null){
		$this->BlogPost->id = $id;
		if ($this->BlogPost->exists())
			$this->set('data',$this->BlogPost->read());
		else
			throw new Exception('Postagem inexistente');
	}
	public function user_add($id = null){
		$this->set("idBlogs",$id);
		if($this->request->is('post')){
			$data = $this->request->data;
			if($this->BlogPost->save($data)){
				$this->Session->setFlash('A postagem foi criada com sucesso.','default', array('class'=>'message success'));
				$this->redirect(array("action"=>"user_index"));
			}else{
				$this->Session->setFlash('A postagem não pôde ser criado. Tente novamente.','default', array('class'=>'message error'));
			}
		}
	}
	public function user_edit($id = null){
		$this->BlogPost->id = $id;
		$post = $this->Utils->verifyEntry($id, "BlogPost");
		$this->set("post",$post);
		if($this->request->is("post") || $this->request->is("put")){
			$data = $this->request->data;
			if($this->BlogPost->save($data)){
				$this->Session->setFlash('A postagem foi editada com sucesso.','default', array('class'=>'message success'));
				$this->redirect(array("action"=>"user_index"));
			}else{
				$this->Session->setFlash('A postagem não pôde ser editada.','default', array('class'=>'message error'));
			}
		}else
			$this->request->data = $this->BlogPost->read();
			$this->set("idBlogs", $this->request->data['BlogPost']['blog_id']);
	}
}