<?php
App::uses('AppController', 'Controller');
class CommentsController extends AppController
{	
	public $uses = array(
		'Comment'
	);
	
	/**
	 * Components
	 */
	public $components = array("Utils");
	
    public function add(){
        if($this->request->is("post")){
            $this->Comment->set($this->request->data);
            if($this->Comment->validates()){
                if($this->Comment->save()){
                    $this->Session->setFlash("Mensagem enviada com sucesso.");
                    $this->Utils->redirectBack();
                } else {
                    $this->Session->setFlash("A Mensagem não pode ser enviada, " 
                                           . "por favor tente novamente.");
                }
            }else{
                $this->Session->setFlash("A Mensagem não pode ser enviada, "
                                       . "certifique-se que todos os campos "
                                       . "foram preenchidos corretamente "
                                       . "antes de enviar novamente.");
                $this->Utils->redirectBack();
            }
        }else{
            throw new MethodNotAllowedException();
        }
    }
	
	/**
	 * Function user_add : Void
	 */
	public function user_add(){
		$this->add();
	}
	
	/**
	 * Function user_edit : Void
	 */
	public function user_edit($id = null){
		if(!$id){
			throw new NotFoundException("Comentário inexistente");
		}
		$comment = $this->Comment->findById($id);
		if(!$comment){
			throw new NotFoundException("Comentário inexistente");
		}
		if($this->request->is("post") || $this->request->is("put")){
			$this->Comment->id = $id;
			if($this->Comment->save($this->request->data)){
				$this->Session->setFlash("Comentário editado com sucesso!");
				$this->Utils->redirectBack();
			}else{
				$this->Session->setFlash("O comentário não pôde "
                                       . "ser editado, tente novamente.");
				$this->Utils->redirectBack();
			}
		}
		if(!$this->request->data){
			$this->request->data = $comment;
		}
	}
	
	/**
	 * Function user_delete : Void
	 * usado para apagar comentários
	 */
	public function user_delete($id)
	{
		if($this->request->is("post")){
			if($this->Comment->delete($id)){
				$this->Session->setFlash("O comentário foi apagado com sucesso!");
				$this->Utils->redirectBack();
			} else {
				$this->Session->setFlash("O comentário não pôde ser apagado");
				$this->Utils->redirectBack();
			}
		} else {
			throw new MethodNotAllowedException();
		}
	}
	
	/**
	 * Function user_multiple : Void
	 * Usado para editar ou apagar multiplos comentários
	 */
	public function user_multiple(){
	    // Veriica se o chamado foi Post
		if($this->request->is("post")){
			$data = $this->request->data;
            // Verifica qual a ação que será feita com os comentários, caso nenhuma, retorna ao remetente
			if($data['Comment']['action'] == "erase"){	
				$action = "erase";
			} else if($data['Comment']['action'] == "toggleActive"){
				$action = "toggleActive";
			} else {
				$this->Session->setFlash("Escolha uma das ações "
				                       . "com os comentários");
				$this->Utils->redirectBack();
			}
			// Interage com os valores recebidos
			foreach($data as $id => $value){
				if($id == "Comment"){
					continue;			
				}
				// Se for apagar
				if($action == "erase"){
					// Verifico se a opção foi marcada
					if($value['Comment']['checked'] == 1){
						// Apague o comentário, se falhar envie a mensagem
						if($this->Comment->delete($id)){
							$this->Session->setFlash("O comentário foi apagado com sucesso!");
						} else {
							$this->Session->setFlash("O comentário não pôde "
						                           . "ser apagado, tente novamente.");
						}
					}
					
				} else { // Se for ativar/desativar
					// Verifica se a opção foi marcada
					if($value['Comment']['checked'] == 1){
						// Define qual comentário deve ser usado
						$this->Comment->id = $id;
                        // Verifica se o comentário está atualmente ativo
						if($this->Comment->field("active")){
							$data = array(
								'active' => false,
							);
						}else{
							$data = array(
								'active' => true,
							);
						}
                        // Salva o comentário, se falhar, envia uma mensagem
						if($this->Comment->save($data)){
							$this->Session->setFlash("O Comentário foi editado "
							                       . "com sucesso!");
						} else {
							$this->Session->setFlash("O Comentário não pôde " 
							                       . "ser editado, por favor "
							                       . "tente novamente");
						}
					}
				}
			}
            $this->Utils->redirectBack();
		} else {
			throw new MethodNotAllowedException();
		}
	}
	
}