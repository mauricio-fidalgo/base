<?php
App::uses('Component', 'Controller');
/**
 * 
 */
class ClearComponent extends Component{
        
    public $components = array("Utils","Session","Auth");
    public $deniedAction = array("user" => false,"controller" => "pages", "action" => "index");
	//public $loginAction = array("user" => true, "controller"=>"dashboard", "action" =>"index");
	public $loginAction = array("controller" => "dashboard", "action" => "index", "user" => true);
	public $logoutAction = array("controller" => "Pages", "action" => "display");
    /**
     * Id do Grupo Visitante
     */
    public $guestGroupId = 3;
    
    /** 
     * Tempo da sessão
     */
    public $sessionTimeOut = '1200000';
    
    public function initialize(Controller $controller){
       $this->controller = $controller;
       $this->User = $controller->User;
       $this->Group = $controller->Group;
    }
    
    public function beforeFilter(){
        if($this->isLogged()){
            if($this->hasTimedOut()){
                $this->endSession();
                $this->Session->setFlash("Sua sessão expirou, execute o login "
                                       . "novamente para continuar a sessão.");
                $this->controller->redirect(array("user"=>false,"controller"=>"pages"
                                                ,"action"=>"display"));
            }else{
                $id = $this->Session->read("Clear.User.id");
                $name = $this->Session->read("Clear.User.username");
                $rules = $this->Session->read("Clear.Group.rules");
                $this->controller->set("sessionClearId",$id);
                $this->controller->set("sessionClearName",$name);
                $this->controller->set("sessionClearRules",$rules);
            }
        }
		//$this->logout();
        if(!$this->isAuthorized() && $this->controller->name != "CakeError"){
            $this->Session->setFlash("Você não está autorizado para "
                                   . "acessar esta página.");
            $this->controller->redirect($this->deniedAction);
        }
    }
    
    /**
     * Loga o usuário ao sistema
     */
    public function login($userData = array()){
        if(!$this->isLogged() || $this->hasTimedOut()){
            $user = $this->authLogin($userData);
            if(!$user){ 
                $this->Session->setFlash("Usuário ou senha incorretas.");
                return false;
            }
            $group = $user['Group'];
            if($this->_compareArray($group) == false){
                $group = $this->Group->findById($user["User"]["group_id"]);
                $group = $group['Group'];
            }
            $user['Group'] = $group;
            $this->beginSession($user, $user);
            $this->Session->setFlash("Usuário logado com sucesso.");
            if(!is_null($group['login_action']) and !empty($group['login_action'])){
            	$link = json_decode($group['login_action'],true);
            	$this->controller->redirect($link);
            }
            else $this->controller->redirect($this->loginAction);
        }else{
            $this->Session->setFlash("Você já está logado no sistema.");
        }
    }
    public function logout(){
        if($this->isLogged()){
            $user = $this->getLoggedUser();
			$this->endSession();
            $this->Session->setFlash("Usuario deslogado com sucesso.");
            if(!is_null($user['Group']['logout_action']) && !empty($user['Group']['logout_action'])){
            	$action = json_decode($user['Group']['logout_action'],true);
            	$this->controller->redirect($action);
            }
            	else
            		$this->controller->redirect($this->logoutAction);
        }
    }
    
    /**
     * Verifica se o usuário e a senha estão corretos para login
     * retorna os dados do usuário se a operação funcionar
     * @return array
     * @return bool
     */
    public function authLogin($userData = Array()){
        if(!$userData) return false;
        $userData = $userData['User'];
        if($userData['username'] && $userData['password']){
        	$this->User->recursive = 1;
            $user = $this->User->find('first',array(
				'conditions' => array(
					'User.username' => $userData['username']
				)
			));
            $userData['password'] = 
                AuthComponent::password($userData['password']);
            if($user && $userData['password'] == $user['User']['password']){
                return $user;
            }else{
                return false;
            }
        }
    }
    /**
	 * 
	 */
	 public function showInfo(){
	 	if($this->isLogged()){
	 		
	 	}
	 }
    /* ========================================================
     * Session Methods
     * ======================================================== */
     /**
      * Inicia a Sessão
      * @return Void
      */
     public function beginSession($userData,$groupData){
        $session = array(
            'User' => array(
                'id' => $userData['User']['id'],
                'username' => $userData['User']['username'],
                'password' => $userData['User']['password'],
                'group_id' => $userData['User']['group_id']
            ),
            'Group' => array(
                'id' => $groupData['Group']['id'],
                'name' => $groupData['Group']['name'],
                'rules' => $groupData['Group']['rules'],
            )
         );
         $this->Session->write("Clear", $session);
         $this->setTimeStamp();
     }
     
     /**
      * Finaliza a sessão
      * @return Void
      */
     public function endSession(){
         $this->Session->delete("Clear");
     }
     
     /**
      * Verifica se o tempo da sessão expirou
      * @return bool
      */
     public function hasTimedOut(){
         $timeStamp = $this->Session->read("Clear.timeStamp");
         if(((time()) - $timeStamp) > $this->sessionTimeOut){
             return true;
         }
         return false;
     }
     
     /**
      * Salva o tempo atual na Session
      * @return Void
      */
     public function setTimeStamp(){
         $this->Session->write("Clear.timeStamp",time());
     }
     
     /**
      * Lê as regras do grupo salvas na session
      * @return Array
      */
     public function getGroupRules(){
         return $this->Session->read("Clear.Group.rules");
     }
     
     /**
      * Verifica se o usuário está logado
      * @return Bool
      */
     public function isLogged(){
         if(!$this->getUserId()){
             return false;
         }else{
             return true;
         }
     }
     
     /**
      * Lê o user id salvo na session
      * @return String
      */
      
     public function getUserId(){
         return $this->Session->read("Clear.User.id");
     }
     
     /**
	  * Confere para ver se o usuário está autorizado a ver esta página
	  */
     public function isAuthorized(){
         $controller = $this->controller->name;
         $action = $this->controller->action;
         if($this->isLogged()){
             $rules = $this->getGroupRules();
         } else {
             $rules = $this->Group->findById($this->guestGroupId);
             $rules = $rules['Group']['rules'];
         }
         if(isset($rules[$controller][$action])){
            if($rules[$controller][$action] == 1) return true;
         }
         return false;
     }
	public function getLoggedUser(){
		if($this->isLogged())
			return $this->User->findById($this->getUserId());
		else
			return false;
	}
    /* ========================================================
     * Private Methods
     * ======================================================== */
    /**
     * Compara o array de controllers com as ações(rules) que o usuário pode 
     * usar, caso o grupo não possua uma das ações ou controllers a função edita
     * e salva de volta na model. Caso o array seja diferente retornara false,
     * se igual retornara true.
     * @param $group - Array resultante do Group->find()
     * @return bool
     */
    private function _compareArray(Array $group){
        $controllers =
            $this->_prepareArray($this->Utils->getAllControllers());
        $rules = $group['rules'];
        foreach($controllers as $controller => $actions){
            if(is_array($rules))
            if(!array_key_exists($controller,$rules)){
                foreach($actions as $action => $value){
                    $rules[$controller][$action] = $value;
                }
                if(!isset($result) || $result == true)
                    $result = false;
            }else{
                foreach($actions as $action => $value){
                    if(!array_key_exists($action,$rules[$controller])){
                        $rules[$controller][$action] = $value;
                        if(!isset($result) || $result == true)
                            $result = false;
                    }
                }
            }
        }
        $group['rules'] = $rules;
        if(isset($result)){
            if($result == false){
                if($this->Group->save($group) == false){
                    $this->Session->setflash("Seu grupo não possui todas as "
                                           . "permissões implementadas, "
                                           . "mas isso não o impede de usar "
                                           . "ações que seu grupo possui.");
                }
            }
        } else $result = true;
        return $result;
    }
    
    /**
     * Prepara array de controllers para comparar com a array salva database
     * @param $controllers = Array()
     */
    private function _prepareArray($controllers = array()){
        foreach($controllers as $controller => $actions){
            foreach($actions as $key => $value){
                unset($controllers[$controller][$key]);
                $controllers[$controller][$value] = 0;
            }
        }
        return $controllers;
    }
}
