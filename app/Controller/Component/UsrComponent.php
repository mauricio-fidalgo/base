<?php
App::uses('Component', 'Controller');
class UsrComponent extends Component{
	public function initialize(Controller $controller){
    	$this->controller = $controller;
    	$this->User = $controller->User;
		if(is_array($this->controller->uses)){
			$this->controller->uses[] = "";
		}
    }
}
