<?php
App::uses('Component', 'Controller');
class UtilsComponent extends Component
{ 
    public $components = array('Session');
     
    /**
     * initialize
     */
    public function initialize(Controller $controller){
        $this->controller = $controller;
    }
    
    /* ========================================================
     * General use Functions
     * ======================================================== */
     
    /**
     * redirectBack
     * Usado para redirecionar ao controller que enviou o pedido
     */
    public function redirectBack(){
        if ($this->controller->referer() != '/') $this->controller->redirect($this->controller->referer());
        else $this->controller->redirect(array('action' => 'index'));
    }
    
    /**
     * verifyEntry
     * Usado para checar se uma inscrição na model existe, se existir,
     * seleciona e retorna todos valores da entrada. Se não existir
     * jogará uma excessão 
     * @param   (int) $id o id para ser checado
     * @param   (string) $model o nome da model para procurar no database
     * @param   (bool) $return define se a array vai ser retornada ou não 
     * (se este argumento for falso a função retornará true se a model existir)
     * @return     (array) $retunrModel 
     * @return     (bool) retorna true se a model existir
     * @throws NotFoundException
     * Retorna a array correspondente ao id com todas as informações
     */
    public function verifyEntry($id,$model,$return = true){
        if(!$id || !$model){
            throw new NotFoundException($model . " inexistente!");
        }
        $returnModel = $this->controller->$model->findById($id);
        if(!$returnModel){
            throw new NotFoundException($model . ' inexistente!');
        }
        if($return){
            return $returnModel;
        } else {
            return true;
        }
    }
    
    /* ========================================================
     * CommentBlock Functions
     * ======================================================== */
    
    /**
    * Prepara as variáveis do elemento "commentElement"
    * @param $id
    */
    public function generateCommentBlock($id){
        if(!$id){
            throw new NotFoundException("Id Inválido");
        }
        $block = $this->CommentBlock->findById($id);
        if(!$block){
            throw new NotFoundException("Id Inválido");
        }
        if($block['CommentBlock']['faceblock'] == 1){
            
        } else {
            $comments = $block['Comments'];
            $commentBlockId = $id;
            return array(
                'comments' => $comments,
                'commentBlockId' => $commentBlockId,
            );
        }
    }
    
    /* ========================================================
     * Options Functions
     * ======================================================== */
     
    /**
     * getOptions
     * Procura as opções associadas com a model
     * @param (string) $model o nome da model
     * @param (int) $id id da model
     * @return array
     */
    public function getOptions($model,$id){
        if(!isset($name)||!isset($id)){
            throw new InvalidArgumentException();
        }
        $result = $this->controller->Option->findAllByModelAndModelId($model,$id);
        if(!$result){
            return;	
        }
        return $result;
    }
    
    /**
     * 
     */
    public function setOption($name,$model,$id,$value){
        if(!isset($name)||!isset($model)||!isset($id)||!isset($value)){
            throw new InvalidArgumentException();
        }
        $find = $this->controller->Option->findByNameAndModelAndModelId($name,$model,$id);
        $value = array(
                    "Option" => array(
                            "name" => $name,
                            "model" => $model,
                            "model_id" => $id,
                            "value" => $value,                            
                        )
                    );
        if($find){
            $this->Option->id = $find['Option']['id'];
            if($this->controller->Option->save($value)){
                $this->Session->setFlash('A configuração foi salva');
            }else{
                $this->Session->setFlash('A configuração não pôde ser salva');
            }
        }else {
            if($this->controller->Option->save($value)){
                $this->Session->setFlash('A configuração foi salva');
            }else{
                $this->Session->setFlash('A configuração não pôde ser salva');
            }
        }
    }

    // Component rewritten, original from : 
    // http://cakebaker.42dh.com/2006/07/21/how-to-list-all-controllers/
    //
    
    /**
     * Return an array of user Controllers and their methods.
     * The function will exclude ApplicationController methods
     * @return array
     */
    public function getAllControllers(){
        $aCtrlClasses = App::objects('controller');
        foreach ($aCtrlClasses as $controller) {
            if ($controller != 'AppController') {
                // Load the controller
                App::import('Controller',
                             str_replace('Controller', '', $controller));
                // Load its methods / actions
                $aMethods = get_class_methods($controller);
                foreach ($aMethods as $idx => $method) {
                    if ($method{0} == '_') {
                        unset($aMethods[$idx]);
                    }
                }
                // Load the ApplicationController (if there is one)
                App::import('Controller', 'AppController');
                $parentActions = get_class_methods('AppController');
                $controller = str_replace('Controller', '', $controller);
                $controllers[$controller] = array_diff($aMethods,
                                                       $parentActions);
            }
        }
        return $controllers;
    }
}
