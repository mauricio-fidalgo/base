<?php
App::uses('AppController','Controller');
/**
 * 
 */
class UsersController extends AppController
{
    public $components = array('Session', 'Utils', 'Clear');
    public $uses = array('User', 'Group');
	public function user_index(){
		$this->paginate = array(
			'User' => array(
				'fields' => array(
					'id',
					'username',
					'group_id',
					'editable',
					'erasable',
					'created',
					'modified'
				),
				'limit' => 20,
			)
		);
		$this->set("users", $this->paginate("User",$recursive = 0));
	}
    public function login(){
    	$this->layout = "gecko_login";
        if($this->request->is('post')){
            $this->Clear->login($this->request->data);
        }
    }
    public function user_login(){
        if($this->request->is('post')){
            $this->Clear->login($this->request->data);
        }
    }
    public function logout(){
        $this->Clear->logout();
    }
    public function user_logout(){
        $this->Clear->logout();
    }
    public function user_add(){
        if($this->request->is('post')){
            $data = $this->request->data;
            if($this->User->save($data)){
                $this->Session->setFlash('O usuário foi criado com sucesso');
            }else{
                $this->Session->setFlash('O usuário não pode ser criado, '
                                       . 'por favor tente novamente');
            }
        }
    }
	public function user_edit($id = null){
		
	}
	public function user_delete($id){
		
	}
}