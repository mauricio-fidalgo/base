<?php
App::uses('AppController','Controller');
class MenuPagesController extends AppController
{
	/**
	 * Components
	 */
	public $components = array("Utils");

    public function user_index(){
        $this->set('menuPages',$this->paginate());
    }
    
    public function user_add(){
        if($this->request->is("post")|| $this->request->is("put")){
        	$saveData = array();
			$requestData = $this->request->data;
        	$saveData["MenuPage"] = $this->request->data["MenuPage"];
			if(array_key_exists("Extra", $requestData)){
				$extraContent = "";
				if(array_key_exists("text", $requestData["Extra"])){
					$extraContent .= "<div class='group3 articles over'>";
					foreach($requestData["Extra"]["text"] as $text){
						$extraContent .= "<div class='col'>";
						$extraContent .= "<h3 class='upper'>&rarr;<span>".$text["title"]."</span></h3>";
						$extraContent .= "<div class='divisor purple med nomarg'></div>";
						$extraContent .= "<p>".$text["content"]."</p>";
						$extraContent .= "</div>";
					}
					$extraContent .= "</div>";
				}
			}
			if($this->MenuPage->save($saveData)){
				$this->Session->setFlash('Página Criada com sucesso!');
                $this->redirect(array('user' => true,'action' => 'index'));
			}else{
                $this->Session->setFlash('A página não pôde ser criada. '
                                       . 'Por favor tente novamente');
			}
        }
		$this->set("prettyPhoto",true);
    }
    
    public function user_view($id = null){
        $data = $this->Utils->verifyEntry($id, 'MenuPage');
        $this->set('data',$data);
    }
    
    public function user_edit($id = null){
        $data = $this->Utils->verifyEntry($id, 'MenuPage');
        if($this->request->is('post') || $this->request->is('put')){
            $saveData = $this->request->data;
			$this->MenuPage->id = $id;
            if($this->MenuPage->save($saveData)){
                $this->Session->setFlash('Página salva com sucesso!');
                $this->redirect(array('user' => true, 'action' => 'index'));
            }else{
                $this->Session->setFlash('A página não pôde ser salva. '
                                       . 'Por favor tente novamente');
            }
        }
		$this->set("prettyPhoto",true);
        $this->request->data = $data;
    }
    
    public function user_delete($id){
        if($this->request->is("post")){
            $this->Utils->verifyEntry($id, "MenuPage",false);
            $this->MenuPage->id = $id;
            if($this->MenuPage->delete()){
                $this->Session->setFlash("A página foi apagada com sucesso!");
            } else {
                $this->Session->setFlash("A página não pôde ser apagada, "
                                       . "tente novamente.");
            }
            $this->Utils->redirectBack();
        }
    }
}