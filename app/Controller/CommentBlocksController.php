<?php
/**
 * 
 */
class CommentBlocksController extends AppController
{
	/**
	 * Components
	 */
	public $components = array("Utils");
	
	public function user_index(){
		$data = $this->paginate("CommentBlock");
		$this->set('data',$data);
	}
    
	public function user_view($id = null){
		$commentBlock = $this->Utils->verifyEntry($id,"CommentBlock");
		$this->set("commentBlock", $commentBlock);
	}
    
    /**
     * Function user_edit : void
     */
	public function user_edit($id = null)
	{
        $commentBlock = $this->Utils->verifyEntry($id,"CommentBlock");
        $this->set("id",$id);
        if($this->request->is("post")){
            $this->CommentBlock->id = $id;
            $data = $this->request->data;
            if($this->CommentBlock->save($data)){
                $this->Session->setFlash("O bolco de comentários foi "
                                       . "editado com sucesso!");
            }else{
                $this->Session->setFlash("O bloco de comentários não "
                                       . "pode ser salvo, por favor "
                                       . "tente novamente.");
            }
        }
        if(!$this->request->data){
            $this->request->data = $commentBlock;
        }
	}
    
	public function user_add(){
		if($this->request->is("post")){
		    if($this->CommentBlock->save($this->request->data)){
		        $this->Session->setFlash("Bloco de comentários criado "
                                       . "com sucesso!");
		    }else{
		        $this->Session->setFlash("O bloco não pôde ser criado, "
                                       . "por favor tente novamente.");
		    }
            $this->redirect(array('action'=>'user_index'));
		}
	}
    
	public function user_delete($id){
		if($this->request->is("post")){
		    if($this->CommentBlock->delete($id)){
		        $this->Session->setFlash("CommentBlock deletado "
		                               . "com sucesso!");
		    }else{
		        $this->Session->setFlash("CommentBlock não pôde "
                                       . "ser deletado, tente novamente.");
		    }
            $this->Utils->redirectBack();
   		}else{
		    throw new MethodNotAllowedException();
		}
	}
}