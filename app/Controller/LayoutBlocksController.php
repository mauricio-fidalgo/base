<?php
App::uses('AppController', 'Controller');
/**
 * LayoutBlocks Controller
 *
 * @property LayoutBlock $LayoutBlock
 * @property UtilsComponent $Utils
 */
class LayoutBlocksController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('General');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Utils');

/**
 * user_index method
 *
 * @return void
 */
	public function user_index() {
		$this->LayoutBlock->recursive = 0;
		$this->set('layoutBlocks', $this->paginate());
	}

/**
 * user_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function user_view($id = null) {
		$this->LayoutBlock->id = $id;
		if (!$this->LayoutBlock->exists()) {
			throw new NotFoundException(__('Invalid layout block'));
		}
		$this->set('layoutBlock', $this->LayoutBlock->read(null, $id));
	}

/**
 * user_add method
 *
 * @return void
 */
	public function user_add() {
		if ($this->request->is('post')) {
			$this->LayoutBlock->create();
			if ($this->LayoutBlock->save($this->request->data)) {
				$this->Session->setFlash(__('The layout block has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The layout block could not be saved. Please, try again.'));
			}
		}
		$layoutSections = $this->LayoutBlock->LayoutSection->find('list');
		$menuPages = $this->LayoutBlock->MenuPage->find('list');
		$this->set(compact('layoutSections', 'menuPages'));
	}

/**
 * user_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function user_edit($id = null) {
		$this->LayoutBlock->id = $id;
		if (!$this->LayoutBlock->exists()) {
			throw new NotFoundException(__('Invalid layout block'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->LayoutBlock->save($this->request->data)) {
				$this->Session->setFlash(__('The layout block has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The layout block could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->LayoutBlock->read(null, $id);
		}
		$layoutSections = $this->LayoutBlock->LayoutSection->find('list');
		$menuPages = $this->LayoutBlock->MenuPage->find('list');
		$this->set(compact('layoutSections', 'menuPages'));
	}

/**
 * user_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function user_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->LayoutBlock->id = $id;
		if (!$this->LayoutBlock->exists()) {
			throw new NotFoundException(__('Invalid layout block'));
		}
		if ($this->LayoutBlock->delete()) {
			$this->Session->setFlash(__('Layout block deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Layout block was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
