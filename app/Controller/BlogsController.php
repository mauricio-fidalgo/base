<?php
App::uses("AppController", "Controller");
/**
 * 
 */
class BlogsController extends AppController
{
    /**
     * Uses
     */
    public $uses = array("BlogPost","Blog");
    
	/**
	 * Components
	 */
	public $components = array("Utils");
	
    /**
     * User index
     */
    public function user_index(){
    	$this->paginate = array(
			"Blog" => array("limit"=>5),
		); 
        $data = $this->paginate("Blog");
        $this->set("data",$data);
    }
    
    /**
     * User add
     */
    public function user_add(){
        if($this->request->is("post")){
            if ($this->Blog->save($this->request->data)){
                $id = $this->Blog->getLastInsertId();
                $this->Utils->setOption("teste", "Blog", $id, 'value');
                $this->Session->setFlash("Blog criado com sucesso!");
                $this->redirect(array("action"=>"index"));
            }else{
                $this->Session->setFlash("O Blog não pôde ser criado,"
                                       . "por favor tente novamente.");
            }
        }
    }
    
    /**
     * User view
     */
    public function user_view($id = null){
        $blog = $this->Utils->verifyEntry($id, "Blog");
        $this->paginate = array(
            "BlogPost" => array(
                "conditions" => array("BlogPost.blog_id" => $id),
                "order" => array("BlogPost.created" => "desc"),
                "limit" => 5,
            ),
        );
        $paginate = $this->paginate();
        $this->set("blog",$blog);
        $this->set("paginate",$paginate);
        $this->set("id",$id);
    }
    
    /**
     * User edit
     */
    public function user_edit($id = null){
        $blog = $this->Utils->verifyEntry($id, "Blog");
        if($this->request->is("post") || $this->request->is("put")){
            $data = $this->request->data;
            $this->Blog->id = $id;
            if($this->Blog->save($data)){
                $this->Session->setFlash("Blog Editado com sucesso");
            } else {
                $this->Session->setFlash("O Blog não pôde ser editado, "
                                       . "tente novamente.");
            }
        } else {
            $this->request->data = $blog;
        }
    }
    
    /**
     * User delete
     */
    public function user_delete($id = null){
        if($this->request->is("post")){
            $blog = $this->Utils->verifyEntry($id, "Blog");
            if($blog['Blog']['erasable']){
                if($this->Blog->delete($id)){
                    $this->Session->setFlash("Blog apagado com sucesso");
					$this->Utils->redirectBack();
                }
            } else {
                $this->Session->setFlash("Este blog não pode ser apagado");
                $this->Utils->redirectBack();
            }
        }else{
            throw new MethodNotAllowedException();
        }
    }
    /**
     * User Multiple Delete
     */
    public function user_multiple_delete(){
        if($this->request->is("post")){
          $requestData  = $this->request->data;
          pr($requestData);
          exit;
        } else
            throw new MethodNotAllowedException();
    }
}
