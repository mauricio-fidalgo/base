<?php
App::uses('AppController', 'Controller');
/**
 * 
 */
class PhotoController extends AppController{
	public $uses = array('Album','Photo');
	public function user_add($id = null){
		$album = $this->Utils->verifyEntry($id,'Album');
		$this->set('album',$album);
		/* Salvando Fotos */
		if ($this->request->is('Post')){
			if(!empty($this->request->data)){
				$data = $this->request->data;
				$data = $data['Photo']['file'];
				foreach($data as $datas => $key){
					$photos['Photo'][$datas]['file'] = $key;
					$photos['Photo'][$datas]['album_id'] = $id;
				}
				if($this->Photo->saveAll($photos['Photo'])){
					$this->Session->setFlash('As Fotos foram salvas com sucesso.','default', array('class'=>'message success'));
				}else{
					$this->Session->setFlash('As Fotos não puderam ser salvas.','default', array('class'=>'message error'));
				}
                $this->redirect(array('controller'=>'Albums','user'=>true,'action'=>'index'));
			}
		}
	}
	public function user_edit($id = null){
		$search = $this->Utils->verifyEntry($id,'Photo');
		$this->set('photo',$search);
		if($this->request->is('post') || $this->request->is('put')){
			$data = $this->request->data;
			$this->Photo->id = $id;
			if($this->Photo->save($data)){
				$this->Session->setflash('Foto editada com sucesso.');
				$this->Utils->redirectBack();
			}else{
				$this->Session->setflash('A foto não pôde ser editado com sucesso.');
				$this->Utils->redirectBack();
			}
		}else{
			$this->request->data = $search;
		}
	}
	public function user_delete($id){
		if($this->Utils->verifyEntry($id, 'Photo', false) && $this->request->is('post')){
			 if($this->Photo->delete($id)){
                 $this->Session->setFlash("A foto foi apagada com "
                                        . "sucesso.");
                 $this->Utils->redirectBack();
             } else {
                 $this->Session->setFlash("A foto não pôde ser apagada, "
                                        . "tente novamente.");
                 $this->Utils->redirectBack();
             }
		}else{
			throw new MethodNotAllowedException();
		}
	}
	public function user_multiple($id = null){
		$this->view = 'select';
		$this->set('prettyPhoto', true);
		$album = $this->Utils->verifyEntry($id, 'Album');
		$this->set('album', $album);
		if($this->request->is('get')){
			$data = $this->request->query;
			if(array_key_exists('data', $data))
				$data = $data['data'];
			if(array_key_exists('Selector', $data)){
				foreach($data['Selector'] as $key => $value){
					$photos[] = $this->Photo->findById($key);
				}
				$this->set('data',$photos);
				$this->set('albumId',$id);
				$this->view = 'editmultiple';
			}
		}
		elseif($this->request->is('post') || $this->request->is('put')){
			$data = $this->request->data;
			if($this->Photo->saveAll($data)){
				$this->Session->setFlash('Fotos salvas com sucesso!');
				$this->redirect(array("user" =>true, "controller"=>"Albums", "action" => "view",$id));
			}
			else $this->Session->setFlash('As fotos não poderam ser salvas');
		}
	}
	public function user_delete_multiple($id){
		$album = $this->Utils->verifyEntry($id,"Album");
		$this->set("album",$album);
		if($this->request->is("post")){
			$selected = $this->request->data["Selector"];
			$error = false;
			$idList = array();
			foreach($selected as $key => $value)
				$idList[] = $key;
			$this->Photo->deleteAll(array('Photo.id' => $idList));
			$this->Session->setFlash("As mensagens foram apagadas com sucesso!");
			$this->redirect(array("controller" => "Albums",
				"action" => "index",
				"user" => true,
				$id
			));
		}
	}
}
