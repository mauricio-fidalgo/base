<?php
App::uses("AppModel", "Model");

/**
 * 
 */
class Group extends AppModel
{
    public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'group_id',
            'depedent' => false
        )
    );
    public function afterFind($results,$primary = false){
        foreach($results as $key => $val){
            if(isset($val['Group']['rules'])){
                $results[$key]['Group']['rules'] = 
                    json_decode($val['Group']['rules'],true);
            }
        }
        return $results;
    }
    public function beforeSave($options = array()){
        if(!empty($this->data['Group']['rules'])){
            $this->data['Group']['rules'] =
                json_encode($this->data['Group']['rules']);
        }
        return true;
    }
}