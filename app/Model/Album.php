<?php
App::uses('AppModel', 'Model');
/**
 * Album
 */
class Album extends AppModel {
	/**
     * name
     */
	public $name = "Album";
	// public $belongsTo = array(
		// "AlbumGroup" => array(
			// 'className' => 'Album_group',
			// 'foreignKey' => 'album_group',
			// 'dependent' => true,
		// ),
	// );
	/**
     * hasMany
     */
	public $hasMany = array(
		'Photo' => array(
			'className' => 'Photo',
			'foreignKey' => 'album_id',
			'dependent' => true,
		)
	);
    /**
     * validate
     */
	public $validate =  array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('minLength', 3),
				'message' => 'Este campo requer no mínimo 3 caracteres.'
			)
		),
	);
}