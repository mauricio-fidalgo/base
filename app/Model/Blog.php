<?php
App::uses('AppModel', 'Model');
/**
 * 
 * 
 */
class Blog extends AppModel
{
    /**
     * hasMany
     */
    public $hasMany = array(
        'BlogPost' => array(
            'className' => 'BlogPost',
            'foreignKey' => 'blog_id',
            'order' => array('created' => 'desc'),
            'dependent' => true
        )
    );
    /**
     * validate
     */
    /*public $validate= array(
        'name' => array(
            'isRequired' => array(
                'rule' => array("minLength" => 3),
                'message' => "O título do blog requer no minimo 3 caracteres."
            ),
        ),
    );*/
}
