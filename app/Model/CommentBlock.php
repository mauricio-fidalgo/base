<?php
App::uses('AppModel', 'Model');
class CommentBlock extends AppModel {
	public $hasMany = array(
		'Comments' => array(
			'name' => 'Comments',
			'order' => array('created' => 'desc'),
			'dependent' => true
		)
	);
}