<?php
App::uses('AppModel', 'Model');
class Photo extends AppModel {
	public $name = "Photo";
	// public $validate = array();
	public $belongsTo = array(
		"Album" => array(
			'className' => 'Album',
			'foreignKey' => 'album_id',
			'dependent' => true,
		),
	);
	public $actsAs = array(
		'Upload.Upload' => array(
			'file' => array(
				'thumbnailMethod' => 'php',
				'thumbnailSizes' => array(
					'xvga' => '1024x768',
					'vga' => '640x480',
					'thumb' => '80x80',
					'display' => '195x195',
					//'display' => '307x205',
					'big' => '128x128',
					'medium' => '64x64',
					'mini' => '32x32'
				),
				'fields' => array(
					'dir' => 'dir',
				),
			),
		),
	);
	public function beforeSave($options = array()){
		if(empty($this->data['Photo']['order'])){
			$order = self::find('first',array(
				'fields' => array('Photo.order'),
				'order' => 'Photo.order desc',
				'conditions'=> array(
					'Photo.album_id =' => $this->data['Photo']['album_id']
				),
			));
			$order = $order['Photo']['order']+1;
			$this->data['Photo']['order'] = $order;
			return true;
		}
	}
}
?>
