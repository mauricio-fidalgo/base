<?php
App::uses('AppModel', 'Model');
/**
 * BlogPost
 * 
 */
class BlogPost extends AppModel
{
    /**
     * belogsTo
     */
    public $belongsTo = array(
        'Blog' => array(
            'className' => 'Blog',
            'dependent' => true
        ),
    );
    
    /**
     * validate
     */
    /*public $validate = array(
        'name' => array(
            'isRequired' => array(
                'rule' => array('minLength' => 3),
                'message' => 'Este campo não pode ficar em branco.',
            ),
        ),
        'message' => array(
            'isRequired' => array(
                'rule' => array('minLength' => 8),
                'message' => 'Este campo não pode ficar em branco.',
            ),
        ),
    );*/
}
