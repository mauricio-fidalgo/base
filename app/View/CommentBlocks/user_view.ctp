<?php echo $this->Html->link("Ver a lista de blocos",array('action'=>'user_index'));?>
<h1>Bloco de comentários</h1>
<div class="comment-block-view-comments">
	<div class="comment-block-view-comments-principal">
		<h1><?php echo $commentBlock['CommentBlock']['name']?></h1>
		<p><?php echo $commentBlock['CommentBlock']['desc']?></p>
		<?php $id = $commentBlock['CommentBlock']['id']; ?>
        <div>
            <h1>Ações</h1>
            <ul>
                <li><?php echo $this->Html->link("Voltar para a lista",array('action'=>'user_index'));?></li>
                <li><?php echo $this->Html->link("Editar Bloco",array('action'=>'user_edit',$id));?></li>
                <li><?php echo $this->Form->postLink("Apagar",array('action'=>'user_delete',$id),
                                                    array(),"Deseja realmente apagar este bloco?");?></li>
            </ul> 
        </div>
        <?php unset($id); ?>
	</div>
	<?php echo $this->Form->create("Comment",array("action" => "user_multiple"));?>
		<?php foreach($commentBlock['Comments'] as $comment): ?>
		<?php $i = $comment['id'];?>
		<div class="comment-block-view-comments-info">
			<p><?php
				echo $this->Form->checkBox("Comment.checked",array(
					'name' => 'data['.$i.'][Comment][checked]',
					'id' => $i."CommentChecked",
				));
				?></p>
				<?php echo $this->Gravatar->image($comment['email'])?>
			<ul>
				<li><h1>Nome: <?php echo $comment['name'] ?></h1></li>
				<li>Status: <?php if($comment['active'] == "0")echo "Desativado"; else echo "Ativado"; ?></li>
				<li>
					<h1>Mensagem:</h1>
					<p><?php echo $comment['message'] ?></p>
				</li>
				<li><p>Criado em: <?php echo $comment['created'];?></p></li>
			</ul>
			<?php /* echo $this->Form->postLink("Apagar mensagem",array('controller'=>'Comments','action'=>'user_delete',$comment['id']),array(),"Deseja realmente apagar esta mensagem?"); */ ?>
		</div>
		<?php endforeach; ?>
		<?php unset($i);?>
		<?php echo $this->Form->label("action","Ação para os itens selecionados");?>
		<?php echo $this->Form->select("action",array(
			'toggleActive' => 'Ativar/Desativar',
			'erase' => 'Apagar',
		));
		?>
		<h1>Info:</h1>
		<ul>
			<li>Ativar/Desativar
				<ul>
					<li>Inverte os valores dos comentários selecionados</li>
					<li>Ativa comentários inativos.</li>
					<li>Desativa comentários ativos.</li>
				</ul>
			</li>
			<li>Apagar
				<ul><li>Apaga os comentários selecionados.</li></ul>
			</li>
		</ul>
	<?php echo $this->Form->end("Enviar");?>
</div>