<div>
    <h1>Ações</h1>
    <ul>
        <li><?php echo $this->Html->link("Voltar para a lista",array('action'=>'user_index'));?></li>
        <li><?php echo $this->Html->link("Ver/Editar Bloco e Comentários",array('action'=>'user_view',$id));?></li>
        <li><?php echo $this->Form->postLink("Apagar",array('action'=>'user_delete',$id),
                                            array(),"Deseja realmente apagar este bloco?");?></li>
    </ul> 
</div>
<?php 
echo $this->Form->create("CommentBlock");
echo $this->Form->input("name",array('label' => 'Nome'));
echo $this->Form->input("desc",array('label' => 'Descrição'));
echo $this->Form->input("faceblock");
echo $this->Form->end("Enviar");
 ?>

