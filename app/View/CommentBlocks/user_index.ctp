<table class="view-table">
	<tr>
		<th><?php echo $this->Paginator->sort("id", "Id");?></th>
		<th><?php echo $this->Paginator->sort("name", "Nome");?></th>
		<th><?php echo $this->Paginator->sort("faceblock", "Faceblock?");?></th>
		<th><?php echo $this->Paginator->sort("created", "Criado");?></th>
		<th><?php echo $this->Paginator->sort("modified", "Modificado");?></th>
		<th>Ações</th>
	</tr>
	<?php foreach($data as $cBlock):?>
	<tr>
		<td><?php echo $cBlock['CommentBlock']['id']?></td>
		<td><?php echo $cBlock['CommentBlock']['name']?></td>
		<td><?php echo $cBlock['CommentBlock']['faceblock']?></td>
		<td><?php echo $cBlock['CommentBlock']['created']?></td>
		<td><?php echo $cBlock['CommentBlock']['modified']?></td>
		<td><?php 
		echo $this->Html->Link("Ver",array('action'=>'user_view',
		                            $cBlock['CommentBlock']['id'])); 
		echo " | ";
		echo $this->Html->Link("Editar",array('action'=>'user_edit',
		                              $cBlock['CommentBlock']['id']));
		echo " | ";	
		echo $this->Form->postLink("Apagar",
		                           array('action'=>'user_delete',
		                           $cBlock['CommentBlock']['id']),
		                           array(),'Deseja realmente apagar este bloco?');	
		?></td>
	</tr>
	<?php endforeach;?>
</table>
<ul>
    <li><?php echo $this->Html->link("Adicionar",array('action'=>'user_add'))?></li>
</ul>