<?php echo $this->General->showTrail(array(
	"Configurações" => array("controller" => "Configuration", "action" => "index", "user" => true),
	"Grupos" => array("controller" => "Groups", "action" => "index", "user" => true),
	"Ver Grupo : " . $data["Group"]["name"]
)); ?>
<h1><?echo $data['Group']['name']?></h1>
<h3>Ações para este grupo</h3>
<?php 
echo $this->Html->link("Editar Grupo", array(
	"controller"=>"Groups",
	"action" => "edit",
	"user" => true,
	$data["Group"]["id"]
), array("class" => "btn btn-normal"));
?>
<h3>Usuários neste grupo</h3>
<ul class="gecko-thumblist">
	<?php foreach($data["User"] as $user):?>
	<li>
		<p>Nome: <b><?php echo $user['username']?></b></p>
		<a class="btn" href="<?php echo $this->Html->url(array("controller" => "Users", "action" => "edit", "user" => true, $user["id"]))?>">Editar</a>
		<a class="btn" href="<?php echo $this->Html->url(array("controller" => "Users", "action" => "delete", "user" => true, $user["id"]))?>">Exclur</a>
	</li>
	<?php endforeach;?>
</ul>
<h3>Permissões</h3>
<ul>
<?php foreach($data["Group"]["rules"] as $name => $controller):?>
	<li>
		<?php echo $name; ?>
		<ul>
		<?php foreach($controller as $action => $value):?>
			<li>
				<?php $value = ($value == 1)? "permitido": "negado";
				echo "Acesso " . $value . " para " . $action; ?>
			</li>
		<?php endforeach;?>
		</ul>
	</li>
<?php endforeach;?>
</ul>