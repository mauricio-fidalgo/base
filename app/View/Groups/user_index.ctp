<?php //pr($data); ?>
<?php
	$trail = array(
		'Configurações' => array(
			'user' => true,
			'controller' => 'Configuration',
			'action' => 'index'),
		'Grupos'
	);
	echo $this->General->showTrail($trail);
?>
<table class="table table-bordered">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort("name", "Nome"); ?></th>
			<th><?php echo $this->Paginator->sort("created", "Criado Em"); ?></th>
			<th><?php echo $this->Paginator->sort("edited", "Editado Em"); ?></th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data as $group):?>
			<tr>
				<td><?php echo $group['Group']['name']?></td>
				<td><?php echo $group['Group']['created']?></td>
				<td><?php echo $group['Group']['modified']?></td>
				<td class="dropdown">
		        	<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
		        		Ações <b class="caret"></b>
					</a>
		        	<ul class="dropdown-menu pull-right">
			        	<li title="Ver">
							<?php echo $this->Html->link("Ver",
								array('action' => 'user_view',
									$group['Group']['id']),
								array('escape' => false));?>
						</li>
						<li title="Editar">
							<?php echo $this->Html->link("Editar",
								array('action' => 'user_edit',
									$group['Group']['id']),
								array('escape' => false));?>
						</li>
						<li title="Apagar">
							<?php if ($group['Group']['erasable'])
								echo $this->Form->postLink("Apagar",
								array('action' => 'delete',
									$group['Group']['id'],'user' => true),
								array('escape' => false),
									"Deseja realmente "
			                  	  . "apagar este Grupo?");?>
			           	</li>
		        	</ul>
	    		</td>
			</tr>
		<?php endforeach;?>
	</tbody>
    <tfoot>
		<tr>
			<td colspan="30">
				<div class="pagination pagination-centered">	
					<ul>
						<?php 
							$first = $this->Paginator->first("<<",array("title"=>"Primeira página"));
							$last = $this->Paginator->last(">>",array("title"=>"Ultima página"));
						?>
						<?php if($first != "")echo "<li>".$first."</li>";else echo "<li><span>&#60;&#60;</span></li>"; ?>
						<li><?php echo $this->Paginator->prev("<",array("escape"=>false,"title"=>"Página anterior"));?></li>
						<?php echo $this->Paginator->numbers(array("tag"=>"li","separator"=>false)); ?>
						<li><?php echo $this->Paginator->next(">",array("escape"=>false,"title"=>"Próxima página"));?></li>
						<?php if($last != "")echo "<li>".$last."</li>";else echo "<li><span>&#62;&#62;</span></li>" ?>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<?php echo $this->Html->link("Criar Grupo", array("controller"=>"Groups", "action"=>"add", "user"=> true),array("class"=>"btn btn-primary"));?>