<div class="rounded title bgwhite g-block g-fullwidth">
	<h1><?php echo $data["MenuPage"]["name"]?></h1>
</div>
<?php if($data["MenuPage"]["subtitle"] != ""):?>
<div class="rounded title bgwhite g-block g-fullwidth">
	<h2><?php echo $data["MenuPage"]["subtitle"]?></h2>
</div>
<?php endif ?>
<?php if($data["MenuPage"]["content"] != ""):?>
<div class="rounded text-block bgwhite g-block g-fullwidth">
	<p><?php echo $data["MenuPage"]["content"]?></p>
</div>
<?php endif ?>
<div class="nav-devcloud text-block bgwhite g-block g-fullwidth normalize-links">
	<div class="nav-devcloud-actions">
		<ul class="inline-menu">
			<li><?php echo $this->Html->link("Editar esta Página",
				array("controller"=>"MenuPages",
					"action"=>"edit",
					"user"=>true,
					$data["MenuPage"]["id"])); ?></li>
			<li><?php echo $this->Html->link("Voltar para a lista de Páginas",
				array("controller"=>"MenuPages",
					"action"=>"index",
					"user"=>true))?></li>
		</ul>
	</div>
</div>