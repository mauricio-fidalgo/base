<?php 
    $Paginator = $this->Paginator;
	$Html = $this->Html;
?>
<div class="bgwhite g-block g-fullwidth title rounded"><h1>Páginas editáveis</h1></div>
<table class="table table-hover table-bordered">
    <thead>
	    <tr class="table-devcloud-header">
	    	<th></th>
	        <th><?php echo $Paginator->sort("name");?></th>
	        <th><?php echo $Paginator->sort("subtitle");?></th>
	        <th>Ações</th>
	    </tr>
    </thead>
    <tbody>
    	<?php foreach($menuPages as $page): ?>
    	<tr>
	    	<td><?php echo $this->Form->checkbox("MenuPage.checked.".$page['MenuPage']['id'])?></td>
	    	<td><?php echo $page["MenuPage"]["name"]?></td>
	    	<td><?php echo $page["MenuPage"]["subtitle"]?></td>
	    	<td class="last">
	    		<span class="batch batch-mini" title="Ver">
					<?php echo $this->Html->link("Ver",
						array('action' => 'user_view',
							$page["MenuPage"]['id']),
						array('escape' => false));?></span>
					<span class="batch batch-mini" title="Editar">
					<?php echo $this->Html->link("Editar",
						array('action' => 'user_edit',
							$page["MenuPage"]['id']),
						array('escape' => false));?></span>
					<span class="batch batch-mini" title="Apagar">
						<?php
							echo $this->Form->postLink("Apagar",
							array('action' => 'user_delete',
								$page["MenuPage"]['id']),
							array('escape' => false),
								"Deseja realmente "
		                  	  . "apagar este Blog "
		                      . "juntamente com "
		                      . "suas postagens?");?></span>
		    </td>
	    </tr>
    	<?php endforeach; ?>
    </tbody>
    <tfoot>
    	<tr class="nav-devcloud">
    		<td colspan="30">
				<div class="pagination pagination-centered">	
					<ul>
						<?php 
							$first = $this->Paginator->first("<<",array("title"=>"Primeira página"));
							$last = $this->Paginator->last(">>",array("title"=>"Ultima página"));
						?>
						<?php if($first != "")echo "<li>".$first."</li>";else echo "<li><span>&#60;&#60;</span></li>"; ?>
						<li><?php echo $this->Paginator->prev("<",array("escape"=>false,"title"=>"Página anterior"));?></li>
						<?php echo $this->Paginator->numbers(array("tag"=>"li","separator"=>false)); ?>
						<li><?php echo $this->Paginator->next(">",array("escape"=>false,"title"=>"Próxima página"));?></li>
						<?php if($last != "")echo "<li>".$last."</li>";else echo "<li><span>&#62;&#62;</span></li>" ?>
					</ul>
				</div>
			</td>
    	</tr>
    </tfoot>
</table>
<?php echo $this->Html->link("Adicionar páginas",
	array("user"=>true,"controller"=>"MenuPages","action"=>"add"),
	array("class"=>"btn btn-primary"))
?>