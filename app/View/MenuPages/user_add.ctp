<div class="title g-block g-fullwidth rounded bgwhite">
    <h1>Adicionar Página</h1>
</div>
<?php echo $this->Form->create("MenuPage"); ?>
<?php echo $this->Form->label("name","Título")?>
<?php echo $this->Form->input("name",array("label" => false,'class'=>'g-block g-twowide')); ?>
<?php echo $this->Form->label("subtitle","Subtítulo"); ?>
<?php echo $this->Form->input("subtitle",array("label" => false,'class'=>'g-block g-twowide')); ?>
<?php echo $this->Form->label("message","Mensagem")?>
<?php echo $this->Form->input("content",array("label"=>false,"class"=>"ckeditor fullwidth")); ?>
<?php echo $this->element("scripts/selectAlbum")?>
<?php echo $this->element("scripts/textList")?>
<?php echo $this->Form->submit("Enviar", array("class"=>"send-form")); ?>
<?php echo $this->Form->end(); ?>
<?php
echo $this->Html->link("Ver o resto das páginas",
	array(
		"user"=>true,
		"controller" => "MenuPages",
		"action" => "index"
	),
	array(
		"class" => "btn btn-primary"
	)
)
?>