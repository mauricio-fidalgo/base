<div class="title g-block g-fullwidth rounded bgwhite">
    <h1>Editar Página</h1>
</div>
<?php echo $this->Form->create("MenuPage"); ?>
<div class="g-block g-fullwidth bgwhite rounded block-padding">
    <table>
        <tr>
            <td class="top-aligned-text">
                <?php echo $this->Form->label("name","Título")?>
            </td>
            <td>
                <?php echo $this->Form->input("name",array("label" => false,'class'=>'g-block g-twowide')); ?>
            </td>
        </tr>
        <tr>
            <td class="top-aligned-text">
                <?php echo $this->Form->label("subtitle","Subtítulo"); ?>
            </td>
            <td>
                <?php echo $this->Form->input("subtitle",array("label" => false,'class'=>'g-block g-twowide')); ?>
            </td>
        </tr>
        <tr>
        	<td class="top-aligned-text"><?php echo $this->Form->label("message","Mensagem")?></td>
        	<td>
        		 <?php echo $this->Form->input("content",array("label"=>false,"class"=>"ckeditor fullwidth")); ?>
        	</td>
        </tr>
    </table>
    <?php echo $this->Form->submit("Enviar", array("class"=>"send-form")); ?>
    <?php echo $this->Form->end(); ?>
</div>
<?php
echo $this->Html->link("Ver o resto das páginas",
	array(
		"user"=>true,
		"controller" => "MenuPages",
		"action" => "index"
	),
	array(
		"class" => "btn btn-primary"
	)
)
?>