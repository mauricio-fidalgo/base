<div class="gecko-login">
	<?php echo $this->Session->flash('flash',array('element'=>'flash_custom'))?>
	<?php
	echo $this->Form->create('User', array('action' => 'login'));
	echo $this->Form->inputs(array(
	    'legend' => __('Login'),
	    'username',
	    'password'
	));
	echo $this->Form->submit('Login',array("class"=>"btn"));
	echo $this->Form->end();
	?>
</div>