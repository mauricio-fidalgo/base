<?php
	$trail = array(
		'Configurações' => array(
			'user' => true,
			'controller' => 'Configuration',
			'action' => 'index'),
		'Usuários'
	);
	echo $this->General->showTrail($trail);
?>
<h1>Usuários</h1>
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('name','Nome')?></th>
			<th><?php echo $this->Paginator->sort('group','Grupo')?></th>
			<th><?php echo $this->Paginator->sort('created','Criado em:')?></th>
			<th><?php echo $this->Paginator->sort('edited','Editado em:')?></th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($users as $user):?>
		<tr>
			<td><?php echo $user['User']['username']?></td>
			<td><?php echo $this->Html->link($user['User']['group_id'],array('controller'=>'Groups','action' => 'view',$user['User']['group_id']))?></td>
			<td><?php echo $user['User']['created']?></td>
			<td><?php echo $user['User']['modified']?></td>
			<td class="dropdown"><a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
	        		Ações <b class="caret"></b>
				</a>
	        	<ul class="dropdown-menu pull-right">
					<?php if(false /*$user['User']['editable'] == 1*/): ?>
					<li title="Editar">
						<?php 
							echo $this->Html->link("Editar",
							array(
								'user' => true,
								'controller' => 'Users',
								'action' => 'edit',
								$user['User']['id']),
							array('escape' => false));
						?>
					</li>
					<?php endif; ?>
					<?php if($user['User']['erasable'] == 1): ?>
					<li title="Apagar">
						<?php
							echo $this->Form->postLink("Apagar",
							array(
								'user' => true,
								'controller' => "Users",
								'action' => 'delete',
								$user['User']['id']),
							array('escape' => false),
								"Deseja realmente "
		                  	  . "apagar este Usuário ?"
							);
						?>
		           	</li>
		           	<?php endif;?>
	        	</ul>
	        </td>
		</tr>
	<?php endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="30">
				<div class="pagination pagination-centered">	
					<ul>
						<?php 
							$first = $this->Paginator->first("<<",array("title"=>"Primeira página"));
							$last = $this->Paginator->last(">>",array("title"=>"Ultima página"));
						?>
						<?php if($first != "")echo "<li>".$first."</li>";else echo "<li><span>&#60;&#60;</span></li>"; ?>
						<li><?php echo $this->Paginator->prev("<",array("escape"=>false,"title"=>"Página anterior"));?></li>
						<?php echo $this->Paginator->numbers(array("tag"=>"li","separator"=>false)); ?>
						<li><?php echo $this->Paginator->next(">",array("escape"=>false,"title"=>"Próxima página"));?></li>
						<?php if($last != "")echo "<li>".$last."</li>";else echo "<li><span>&#62;&#62;</span></li>" ?>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<div>
	<?php echo $this->Html->link("Cadastrar um novo usuário", array('user'=> true,'controller' =>'Users', 'action'=>'add'), array('class'=>'btn btn-primary'));?>
</div>