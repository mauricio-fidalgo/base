<?php
	$trail = array(
		'Álbuns'
	);
	echo $this->General->showTrail($trail);
?>
<h1>Álbuns</h1>
<?php
$comentario = true;
if(!$comentario):
?>
<?php echo $this->Form->Create('Album')?>
<table class='table table-hover table-bordered'>
	<thead>
		<tr>
			<th> </th>
			<th><?php echo $this->Paginator->sort('name','Título');?></th>
			<th><?php echo $this->Paginator->sort('subtitle','Subtítulo');?></th>
			<th><?php echo $this->Paginator->sort('slide','É slide?');?></th>
			<th>N° de fotos:</th>
			<th><?php echo $this->Paginator->sort('created','Criado em');?></th>
			<th>Ações:</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($albums as $album):?>
		<tr>
			<td><?php echo $this->Form->checkbox('Albuns.checked.'.$album['Album']['id']);?></td>
			<td><?php echo $this->Html->link($album['Album']['name'],array('action' => 'user_view',$album['Album']['id']))?></td>
			<td><?php echo $album['Album']['subtitle']?></td>
			<td><?php echo ($album['Album']['slide'] == 1)?'Sim':'Não';?></td>
			<td><?php echo $album['Album']['qtd']?></td>
			<td><?php echo $album['Album']['created']?></td>
			<td class="dropdown">
				<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">Ações <b class="caret"></b></a>
				<ul class="dropdown-menu pull-right">
					<li title="Ver">
					<?php echo $this->Html->link('Ver',
						array('action' => 'user_view',
							$album['Album']['id']),
						array('escape' => false));?></li>
					<li title="Editar">
					<?php echo $this->Html->link('Editar',
						array('action' => 'user_edit',
							$album['Album']['id']),
						array('escape' => false));?></li>
					<li title="Adicionar Imagens">
					<?php echo $this->Html->link('Adicionar Imagens',
						array('controller'=>'Photo',
							'action' => 'add',
							'user'=>true,
							$album['Album']['id']),
						array('escape' => false));?></li>
					<li title="Apagar Imagens">
						<?php echo $this->Html->link('Apagar Imagens',
						array('controller'=>'Photo',
							'action' => 'delete_multiple',
							'user' => true,
							$album['Album']['id']),
						array('escape' => false));?></li>
					<li title="Apagar">
						<?php if ($album['Album']['erasable'] == 1)
							echo $this->Form->postLink('Apagar este Álbum',
							array('action' => 'user_delete',
								$album['Album']['id']),
							array('escape' => false),
							'Deseja relmente apagar este album? Uma vez apagado o Album não podera ser restaurado e suas imagens serão perdidas.');?></li>
				</ul>
			<?php
			/*
			echo " | ";
			echo $this->Html->link("Adicionar Fotos",array('action' => 'user_add_images',$album['Album']['id']));
			echo " | ";
			echo $this->Html->link("Apagar Fotos", array('action' => 'user_delete_images',$album['Album']['id']));
			if ($album['Album']['erasable'] == 1){
				echo " | ";
				echo $this->Form->postLink("Apagar",array('action' => 'user_delete',$album['Album']['id']),null,"Deseja relmente apagar este album? Uma vez apagado o Album não podera ser restaurado e suas imagens serão perdidas.");
			}*/
			?></td>
		</tr>
	</tbody>
	<?php endforeach;?>
	<tfoot>
		<tr>
			<td colspan="30">
				<div class="pagination pagination-centered">	
					<ul>
						<?php 
							$first = $this->Paginator->first('<<',array('title'=>'Primeira página'));
							$last = $this->Paginator->last('>>',array('title'=>'Ultima página'));
						?>
						<?php if($first != '')echo '<li>'.$first.'</li>';else echo '<li><span>&#60;&#60;</span></li>'; ?>
						<li><?php echo $this->Paginator->prev('<',array('escape'=>false,'title'=>'Página anterior'));?></li>
						<?php echo $this->Paginator->numbers(array('tag'=>'li','separator'=>false)); ?>
						<li><?php echo $this->Paginator->next('>',array('escape'=>false,'title'=>'Próxima página'));?></li>
						<?php if($last != '')echo '<li>'.$last.'</li>';else echo '<li><span>&#62;&#62;</span></li>' ?>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<?php endif;?>
<?php
	$table = array();
	$albumSize = count($albums);
	for($i = 0; $i < $albumSize; $i++){
		$table[$i] = $albums[$i]['Album'];
	}
	$tableUrlEdit = $this->General->tableUrl(array(
		'controller' => 'Albums',
		'action' => 'edit',
		'user' => true
	),'id');
	$tableUrlView = $this->General->tableUrl(array(
		'controller' => 'Albums',
		'action' => 'view',
		'user' => true
	),'id');
	$tableUrlAddImages = $this->General->tableUrl(array(
		'controller' => 'Photo',
		'action' => 'add',
		'user' => true
	),'id');
	$tableUrlEraseImages = $this->General->tableUrl(array(
		'controller' => 'Photo',
		'action' => 'delete_multiple',
		'user' => true,
	),'id');
$optCol = <<<EOT
<div class="dropdown">
	<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">Ações <b class="caret"></b></a>
	<ul class="dropdown-menu pull-right">
		<li><a href="$tableUrlView">Ver</a></li>
		<li><a href="$tableUrlEdit">Editar</a></li>
		<li><a href="$tableUrlAddImages">Adicionar Imagens</a></li>
		<li><a href="$tableUrlEraseImages">Apagar Imagens</a></li>
	</ul>
</div>
EOT;
	$opt = array(
		'Table' => array(
			'class' => 'table table-hover table-bordered'
		),
		'Foot' => array('Pagination'),
		'Header' => array(
			'',
			$this->Paginator->sort('name','Título'),
			$this->Paginator->sort('subtitle','Subtítulo'),
			//$this->Paginator->sort('slide','É slide?'),
			'N° de fotos:',
			$this->Paginator->sort('created','Criado em'),
			'Opções'
		),
		'UseKeys' => array(
			'',
			'name',
			'subtitle',
			//'slide',
			'qtd',
			'created',
		),
		'Extras' => array(
			$optCol,
		)
	);
	echo $this->General->generateTable($table,$opt);
?>
<div>
	<?php echo $this->Html->link('Criar mais albuns', array('user'=> true,'controller' =>'Albums', 'action'=>'add'), array('class'=>'btn btn-primary'));?>
</div>
<?php echo $this->Form->end(); ?>
