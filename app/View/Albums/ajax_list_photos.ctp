<?php if(isset($albumList)):?>
	<?php echo $albumList; ?>
<?php else:?>
	<?php if(!empty($album['Photo'])):?>
		<?php $photos = $album['Photo']?>
		<div class="thumbnails">
		<?php foreach($photos as $photo):?>
			<div class="span3">
				<div class="thumbnail">
					<?php 
					$url = array('model'=>'Photo','file'=>$photo['file'],'id'=>$photo['id'],"size"=>"mini");
					$image = $this->DataFotos->show($url);
					$to = array('model'=>'Photo','file'=>$photo['file'],'id'=>$photo['id']);
					echo $this->DataFotos->prettyPhoto($image,$to,array('rel'=>'prettyPhoto[]'))?>
					<?php $url['size'] = ''?>
					<input class="span2" type="text" value="<?php echo $this->DataFotos->getUrl($url);?>" disabled="true" style="cursor:text;"/>
				</div>
			</div>
		<?php endforeach;?>
		</div>
	<?php else:?>
		<h3>Este álbum não contem fotos</h3>
	<?php endif;?>
<?php endif;?>