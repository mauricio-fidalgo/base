<?php
	$trail = array(
		'Álbuns' => array(
			'controller' => 'Albums',
			'action' => 'index',
			'user' => true
		),
		'Álbum - '.$album['Album']['name']);
	echo $this->General->showTrail($trail);
?>
<div>
	<h1><?php echo $album['Album']['name']?></h1>
</div>
<?php if($album['Album']['subtitle'] != ""): ?>
<div>
	<h2><?php echo $album['Album']['subtitle']?></h2>
</div>
<?php endif; ?>
<?php if($album['Album']['desc'] != ""): ?>
<div>
	<p><?php echo $album['Album']['desc']?></p>
</div>
<?php endif; ?>
<?php
	$links = array("Adicionar Fotos" => array("controller"=>"Photo","user" =>true,"action"=>'user_add',$album['Album']['id']),
		"Editar Fotos"=>array("controller"=>"Photo","user" => true, "action"=>'multiple',$album['Album']['id']),
		"Apagar Fotos"=>array("controller"=>"photo","user" => true, "action" => "delete_multiple", $album['Album']['id']),
		"Editar informações do album" => array("action"=>"edit","user"=>true,$album['Album']['id']),
		"Voltar para albuns" => array('action'=>'index',"user"=>true)
	);
	echo $this->General->navList($links,"Ações");
?>
<ul class="thumbnails">
<?php foreach($album['Photo'] as $foto):?>
	<li class="span3">
		<div class="thumbnail">
			<?php echo $this->DataFotos->show(array(
					'model' => 'Photo',
					'id' => $foto['id'],
					'file' => $foto['file'],
					'size' => 'display'),array(
					'class' => 'rounded',
					'title'=>$foto['name']
					));
			?>
			<div class="caption">
				<?php if(!empty($foto['name'])):?>
					<h3><?php echo $foto['name']?></h3>
				<?php endif;?>
				<?php if(!empty($foto['desc'])):?>
					<p><?php echo $foto['desc']?></p>
				<?php endif;?>
				<?php echo $this->DataFotos->prettyPhoto("Ver",
					array('model' => 'Photo',
						'id' => $foto['id'],
						'file' => $foto['file']),
					array('escape' => false,
						 'class'=> 'btn',
						 'rel' => 'prettyPhoto[gal='.$album['Album']['name'].']')); ?>
				<?php echo $this->Html->link("Editar",
					array('controller'=>'Photo',
						'action' => 'edit',
						'user'=> true,
						$foto['id']),array('escape'=>false,
						 'class'=> 'btn'));?>
				<?php echo $this->Form->postLink('Apagar',
					array('controller' => 'Photo',
						'action' => 'delete',
						'user' => true,
						$foto['id']),
						array('escape' => false,
						'class'=> 'btn btn-danger'),
						'Deseja realmente apagar esta imagem?')?>
			</div>
		</div>
	</li>
<?php endforeach;?>
</ul>