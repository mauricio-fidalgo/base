<?php if(!empty($album['Photo'])): ?>
<?php echo $this->Form->create("Photo");?>
<div>
	<?php foreach($album['Photo'] as $foto):?>
	<div>
		<?php echo $this->DataFotos->show(array(
				'model' => 'Photo',
				'id' => $foto['id'],
				'file' => $foto['file'],
				'size' => 'thumb'));
				echo $this->Form->checkbox("Foto.marcado",array(
				'name'=>'data[Photo]['.$foto['id'].']',
				'hiddenField'=>false,
				));
				echo "Título: ".$foto['name'];
		?>
	</div>
	<?php endforeach;?>
</div>
<?php echo $this->Form->end("Apagar Fotos selecionadas");?>
<?php else: ?>
	<p>Esse Album ainda não tem fotos.</p>
<?php endif; ?>