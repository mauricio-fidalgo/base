<h1><?php echo $album['Album']['name']?></h1>
<p><?php echo $album['Album']['subtitle']?></p>
<p><?php echo $album['Album']['desc']?></p>
<div>
	<?php
		/*
		echo $this->Html->link("Adicionar Fotos",array("action"=>'user_add_images',$album['Album']['id']));
		echo " | ";
		echo $this->Html->link("Editar Fotos",array("action"=>'user_edit_images',$album['Album']['id']));
		echo " | ";
		echo $this->Html->link("Apagar Imagens",array('action'=>'user_delete_images',$album['Album']['id']));
		echo " | ";
		echo $this->Html->link("Voltar para albuns",array('action'=>'user_index'));
		*/
	?>
</div>
<?php 
if(!$selected && !empty($album['Photo'])):
echo $this->Form->create("Foto");
echo $this->Form->hidden("Foto.tipo",array('value'=>'selector'));
?>
<div>
	<?php foreach($album['Photo'] as $foto):?>
	<div>
		<?php echo $this->DataFotos->show(array(
				'model' => 'Photo',
				'id' => $foto['id'],
				'file' => $foto['file'],
				'size' => 'thumb'));
				echo $this->Form->checkbox("Foto.marcado",array(
				'name'=>'data[Photo]['.$foto['id'].'][]',
				'hiddenField'=>false,
				));
		?>
	</div>
	<?php endforeach;?>
</div>
<?php 
echo $this->Form->end("Editar fotos selecionadas");
elseif(!empty($album['Photo'])):
	echo $this->Form->create("Photo"); 
	echo $this->Form->hidden("Foto.tipo",array('value'=>'save'));
	$i = 0;
	foreach($data as $photo):?>
	<div>
		<?php echo $this->DataFotos->show(array(
				'model' => 'Photo',
				'id' => $photo['Photo']['id'],
				'file' => $photo['Photo']['file'],
				'size' => 'vga'
			),array('width' => 400,'height' => 270));?>
		<?php echo $this->Form->hidden("Photo.id",array(
			'name' => 'data['.$i.'][Photo][id]',
			'value' => $photo['Photo']['id'],
			'id' => $i."PhotoId",
		));?>
		<?php echo $this->Form->input("Photo.name",array(
			'name' => 'data['.$i.'][Photo][name]',
			'value' => $photo['Photo']['name'],
			'id' => $i."PhotoName",
		));?>
		<?php echo $this->Form->input("Photo.desc",array(
			'name' => 'data['.$i.'][Photo][desc]',
			'value' => $photo['Photo']['desc'],
		));?>
		<?php echo $this->Form->input("Photo.order",array(
			'name' => 'data['.$i.'][Photo][order]',
			'value' => $photo['Photo']['order'],
			'id' => $i."PhotoOrder",
		));?>
		<?php echo $this->Form->input("Photo.active",array(
			'name' => 'data['.$i.'][Photo][active]',
			'id' => $i."PhotoActive",
			($photo['Photo']['active'] == 1)?'checked':null,
		));?>
		<?php if($photo['Photo']['erasable'] == 1){
			echo "<div>";
			echo $this->Form->checkBox("Photo.erase",array(
				'name' => 'data['.$i.'][Photo][erase]',
				'id' => $i."PhotoErase",
				));
			echo $this->Form->label($i.'Photo.erase','Apagar?');
			echo "</div>";
		}?>
	</div>
	<hr/>
<?php
	$i++;
	endforeach;
	unset($i);
	echo $this->Form->end("Enviar"); 
else: ?>
	<p>Esse Album ainda não tem fotos.</p>
<?php endif; ?>
<div class="g-block g-fullwidth bgwhite title rounded">
	<h1><?php echo $album['Album']['name']?></h1>
</div>
<?php if($album['Album']['subtitle'] != ""): ?>
<div class="g-block g-fullwidth bgwhite title rounded">
	<h2><?php echo $album['Album']['subtitle']?></h2>
</div>
<?php endif; ?>
<?php if($album['Album']['desc'] != ""): ?>
<div class="g-block g-fullwidth bgwhite text-block rounded">
	<p><?php echo $album['Album']['desc']?></p>
</div>
<?php endif; ?>
<div class="nav-devcloud bgwhite rounded text-block g-block g-fullwidth normalize-links">
	<ul class="inline-menu nav-devcloud-actions">
		<li><?php echo $this->Html->link("Adicionar Fotos",array("action"=>'user_add_images',$album['Album']['id']));?></li>
		<li><?php echo $this->Html->link("Voltar para o Album",array("action"=>'user_view',$album['Album']['id']));?></li>
		<li><?php echo $this->Html->link("Voltar para albuns",array('action'=>'user_index'));?></li>
	</ul>
</div>
<?php 
echo $this->Form->create("Foto");
echo $this->Form->hidden("Foto.tipo",array('value'=>'selector'));
$i = 1;
$j = 0;
?>
<?php foreach($album['Photo'] as $foto):?>
	<?php if($i > 4)$i=1; ?>
	<div class="g-block g-235 bgwhite rounded margin display-photo <?php if($i == 1)echo "left"; else if($i == 4) echo "right";?>">
		<?php echo $this->DataFotos->show(array(
				'model' => 'Photo',
				'id' => $foto['id'],
				'file' => $foto['file'],
				'size' => 'display'),array(
				'class' => 'rounded'
				));
			$i++;
		?>
		<div class="display-photo-selector">
			<?php echo $this->Form->checkbox("Foto.marcado",array(
				'name'=>'data[Photo]['.$foto['id'].'][]',
				'hiddenField'=>false,
				)); ?>
		</div>
		<ul class="display-photo-actions inline-menu batch normalize-links">
			<li class="batch-mini">
				<?php echo $this->DataFotos->prettyPhoto("&#xf178;",
					array('model' => 'Photo',
						'id' => $foto['id'],
						'file' => $foto['file']),
					array('escape' => false,
						 'rel' => 'prettyPhoto[gal='.$album['Album']['name'].']')); ?>
			</li>
		</ul>
	</div>
	<?php $j++;?>
<?php endforeach;?>
<?php echo $this->Form->end();?>