<div>
	<h1><?php echo $data["BlogPost"]["name"]?></h1>
</div>
<?php if($data["BlogPost"]["subtitle"] != ""):?>
<div>
	<h2><?php echo $data["BlogPost"]["subtitle"]?></h2>
</div>
<?php endif ?>
<?php if($data["BlogPost"]["call"] != ""):?>
<div>
	<p><?php echo $data["BlogPost"]["call"]?></p>
</div>
<?php endif ?>
<?php if($data["BlogPost"]["message"] != ""):?>
<div>
	<p><?php echo $data["BlogPost"]["message"]?></p>
</div>
<?php endif ?>
<?php
	$nav = array(
		"Editar este Post" => array(
			"controller"=>"BlogPosts",
			"action"=>"edit",
			"user"=>true,
			$data["BlogPost"]["id"]),
		"Voltar para a lista de Posts" => array(
			"controller"=>"Blogs",
			"action"=>"view",
			"user"=>true,
			$data["BlogPost"]["blog_id"])
	);
	echo $this->General->navList($nav);
?>