<?php echo $this->Form->create("BlogPost"); ?>
<?php echo $this->Form->hidden("blog_id");?>
<fieldset>
	<legend>Adicionar Postagem</legend>
	<?php echo $this->Form->label("name","Título")?>
    <?php echo $this->Form->input("name",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("subtitle")?>
  	<?php echo $this->Form->input("subtitle",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("call","Descrição"); ?>
    <?php echo $this->Form->input("call",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("message","Mensagem")?>
    <?php echo $this->Form->input("message",array("label"=>false,"class"=>"ckeditor")); ?>
    <?php echo $this->Form->submit("Enviar", array("class"=>"send-form")); ?>
    <?php echo $this->Form->end(); ?>
</fieldset>
<?php
echo $this->General->navList(
	array(
		"Voltar para o blog"=>array(
    		"user" => true,
        	"controller" => "Blogs",
        	"action" => "view",
        	$idBlogs)
   	)
);
?>