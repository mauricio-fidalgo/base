<?php echo $this->Form->create("BlogPost"); ?>
<?php echo $this->Form->hidden("blog_id");?>
<fieldset>
	<legend>Editar Postagem</legend>
	<?php echo $this->Form->label("name","Título")?>
    <?php echo $this->Form->input("name",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("subtitle","Subtítulo")?>
  	<?php echo $this->Form->input("subtitle",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("call","Descrição"); ?>
    <?php echo $this->Form->input("call",array("label" => false,'class'=>'g-block g-twowide')); ?>
    <?php echo $this->Form->label("message","Mensagem")?>
    <?php echo $this->Form->input("message",array("label"=>false,"class"=>"ckeditor fullwidth")); ?>
    <?php echo $this->Form->submit("Salvar Edições", array("class"=>"btn")); ?>
    <?php echo $this->Form->end(); ?>
</fieldset>
<?php
echo $this->General->navList(
	array(
		"Voltar para o blog"=>array(
    		"user" => true,
        	"controller" => "Blogs",
        	"action" => "view",
        	$idBlogs)
   	)
);
?>