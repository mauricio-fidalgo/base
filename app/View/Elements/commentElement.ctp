<?php
/*
 * Antes de usar este Element defina de qual bloco o comentário pertence
 * com a variável $comment_block_id.
 * 
 * Defina a array de comentários na variável $comments antes de usar este
 * element.
 */
?>
<div class="element-comment-block">
	<?php foreach($comments as $comment):?>
		<div class="element-comment-block-message">
			<div class="element-comment-block-message-gravatar">
				<?php echo $this->Gravatar->image($comment['email']);?>
			</div>
			<div class="element-comment-block-message-info">
				<ul>
					<li><span>Nome:</span> <?php echo $comment['name']?></li>
				</ul>
			</div>
			<div class="element-comment-block-message-text">
				<h1>Comentário:</h1>
				<p><?php echo $comment['message']?></p>
			</div>
		</div>
	<?php endforeach;?>
	<div class="element-comment-block-form">
		<?php echo $this->Form->create('Comment', array(
				'inputDefaults' => array(
					'label' => false,
					'div' => false,
				),
				'url' => array('controller' => 'Comments', 'action' => 'add'),
			)); ?>
			<?php echo $this->Form->hidden("Comment.comment_block_id",array('value'=>$commentBlockId));?>
			<div class="element-comment-block-form-trace">
				<?php echo (isset($validationErrors))? var_dump($validationErrors):""; ?>
			</div>
			<div class="element-comment-block-form-input">
				<?php 
				echo $this->Form->label("Comment.name","Nome");
				echo $this->Form->input("Comment.name");
				?>
			</div>
			<div class="element-comment-block-form-input">
				<?php 
				echo $this->Form->label("Comment.email","E-Mail");
				echo $this->Form->input("Comment.email");
				?>
				<p>Seu E-Mail não será revelado no site.</p>
			</div>
			<div class="element-comment-block-form-input">
				<?php 
				echo $this->Form->label("Comment.message","Mensagem");
				echo $this->Form->input("Comment.message");
				?>
			</div>
			<div class="element-comment-block-form-submit">
				<?php echo $this->Form->submit("Enviar");?>
			</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>