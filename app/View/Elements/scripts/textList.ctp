<formset id="script-TextList-selector" class="hidden-ajax">
	<label for="script-TextList-check">
		<input id="script-TextList-check" type="checkbox">
		Adicionar uma lista de textos.
	</label>
	<div id="script-TextList-gp1" class="hidden well">
		<a id="script-TextList-addText" href="javascript:void(0)" class="btn btn-success">+ Adicionar Texto</a>
	</div>
</formset>
<script>
$(document).ready(function(){
	var pre = "#script-TextList-";
	var x = 0; // numero de textos
	$(pre+"selector").removeClass("hidden-ajax");
	$(pre+"check").change(function(){
		$(pre+"gp1").toggle(this.checked);
	}).change();
	$(pre+"addText").click(function(){
		if(x == 0){
			$(pre+"gp1").append("<div id='script-TextList-textAdded'></div>");
		}
		var text = document.createElement("div");
		var close = document.createElement("a");
		text.id = "script-TextList-textAdded-" + x;
		close.className = "pull-right btn btn-danger btn-small";
		close.innerHTML = "X";
		text.innerHTML = "<label>Insira o título*:</label><input name='data[Extra][text]["+x+"][title]' type='text'>";
		text.innerHTML += "<label>Texto:</label><textarea name='data[Extra][text]["+x+"][content]'></textarea>";
		text.innerHTML += "<label>URL:</label><input name='data[Extra][text]["+x+"][url]' type='text'>";
		text.className = "well";
		$(pre+"textAdded").append(text);
		$(text).prepend(close);
		var _text = $(text);
		$(close).click(function(){$(_text).remove()});
		x++;
	});
});
</script>