<formset id="script-SelectAlbum-selector" class="hidden-ajax">
	<label for="script-SelectAlbum-selector-select">
		<input type="checkbox" id="script-SelectAlbum-selector-select"><strong> Deseja Adicionar Fotos de um dos albuns a esta página?</strong>
	</label>
	<div id="script-SelectAlbum-selector-sub1" class="hidden well">
		<label for="script-SelectAlbum-selector-sub1-add">
			<input type="checkbox" id="script-SelectAlbum-selector-sub1-add"/> Adicionar um Álbum
		</label>
		<div id="script-SelectAlbum-selector-sub1-1" class="well">
			<div class="alert alert-info">
				<p>Esta modificação não é visivel nesta página, só pode ser vista no site.</p>
				<p>Esta modificação serve para mostrar um album inteiro na página.</p>
			</div>
			<select name="data[MenuPage][album_id]" class="script-SelectAlbum-albumList">
				
			</select>
		</div>
		<label for="script-SelectAlbum-selector-sub1-select">
			<input type="checkbox" id="script-SelectAlbum-selector-sub1-select"/> Escolher fotos para o texto
		</label>
		<div id="script-SelectAlbum-selector-sub1-2" class="well">
			<div class="alert alert-info">
				<p>Para usar estas fotos na página siga os seguintes passos:</p>
				<ul>
					<li>Escolha em qual Álbum você deseja selecionar imagens</li>
					<li>Escolha o tamanho da imagem que você deseja exibir</li>
					<li>Selecione e copie a url (texto em cinza)</li>
					<li>No editor de texto clique no ícone da Paisagem (<span style="background:url('<?php echo $this->webroot; ?>img/icons.png'); background-position:0 -1504px; height:16px;width:16px; display:inline-block;"></span>)</li>
					<li>Cole a url da imagem no campo URL do formulário</li>
				</ul>
			</div>
			<select id="script-SelectAlbum-selector-sub1-2-list" class="script-SelectAlbum-albumList">
				<option value="0">Selecione o álbum</option>
			</select>
			<div id="script-SelectAlbum-div">
			</div>
		</div>
	</div>
</formset>
<?php 
/*
 * Javascript
 */
?>
<script>
(function(){
	var baseId,firstTime,albumList;
	baseId = "#script-SelectAlbum-";
	albumList = null;
	(function(){
		$.ajax({
			url:"<?php echo $this->Html->url(array("controller"=>"Albums","action"=>"ajax_listPhotos","user"=>false))?>",
			error: function(err){
				$(baseId+"selector")
					.removeClass("hidden-ajax")
					.html(err);
			}
		}).done(function(data){
			albumList = jQuery.parseJSON(data);
			//console.log(albumList);
			var teste = jQuery.each(albumList,function(key,value){
				var album;
				album = value['Album'];
				$(".script-SelectAlbum-albumList")
					.append("<option value="+album.id+">"+album.name+"</option>");
			});
			$(baseId+"selector").removeClass("hidden-ajax");
		});
	})()
	$(document).ready(function(){
		$(baseId+"selector-select").change(function(){
			$(baseId+"selector-sub1").toggle(this.checked);
			if(!this.checked){
				$(baseId+"selector-sub1-add").attr("checked",false);
				$(baseId+"selector-sub1-1").toggle(this.checked);
			}
		}).change();
		$(baseId+"selector-sub1-add").change(function(){
			$(baseId+"selector-sub1-1").toggle(this.checked);
		}).change();
		$(baseId+"selector-sub1-select").change(function(){
			$(baseId+"selector-sub1-2").toggle(this.checked);
		}).change();
		$(baseId+"selector-sub1-2-list").change(function(){
			if($(baseId+"selector-sub1-2-list").val() != 0)
				$.ajax({
				url:"<?php echo $this->Html->url(array("controller"=>"Albums","action"=>"ajax_listPhotos","user"=>false))?>/"+$(baseId+"selector-sub1-2-list").val(),
				beforeSend: function(){
					$(baseId+"div").addClass("hidden-ajax");
				},
				success: function(data){
					$(baseId+"div").removeClass("hidden-ajax");
					$(baseId+"div").html(data);
					$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});
				},
				error: function(data,err,mess){
					$(baseId+"div").removeClass("hidden-ajax")
						.html("<div class='alert alert block'><p>O álbum não pôde ser carregado no momento, atualize a página para corrigir esse problema, se o erro persistir entre em contato com o servidor.</p><p></div>"+mess+"</p>")
						.append(data.responseText);
					console.log(data);
				},
				cache: true,
				});
		}).change();
	});
})()
</script>