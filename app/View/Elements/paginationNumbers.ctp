<div class="nav-devcloud-numbers">
    <span class="batch batch-mini"><?php echo $this->Paginator->first("&#xf168;",array("escape"=>false,"title"=>"Primeira página"));?></span>
	<span class="batch batch-mini"><?php echo $this->Paginator->prev("&#xf169;",array("escape"=>false,"title"=>"Página anterior"));?></span>
	<span class="numbers"><?php echo $this->Paginator->numbers(); ?></span>
	<span class="batch batch-mini"><?php echo $this->Paginator->next("&#xf16d;",array("escape"=>false,"title"=>"Próxima página"));?></span>
	<span class="batch batch-mini"><?php echo $this->Paginator->last("&#xf16e;",array("escape"=>false,"title"=>"Ultima página"));?></span>
</div>