<?php
	$albumId = $album['Album']['id'];
	$trail = array(
		'Álbuns' => array(
			'controller' => 'Albums',
			'action' => 'index',
			'user' => true
		),
		'Álbum - '.$album['Album']['name'] => array(
			'controller' => 'Albums',
			'action' => 'view',
			'user' => true,
			$albumId
		),
		'Adicionar Fotos'
	);
	echo $this->General->showTrail($trail);
?>
<h1>Adicionar Fotos</h1>
<div class="text-block bgwhite g-block g-fullwidth rounded">
	<p>Escolha os Arquivos para enviar</p>
	<?php echo $this->Form->create('Photo', array('type' => 'file')); ?>
	    <?php echo $this->Form->input('Photo.file', array('type' => 'file', 'multiple','name' => 'data[Photo][file][]','label' =>false)); ?>
		<?php echo $this->Form->submit("enviar")?>
	<?php echo $this->Form->end(); ?>
</div>