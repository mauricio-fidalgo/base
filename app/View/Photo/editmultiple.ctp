<?php echo $this->Form->create("Photo");?>
<?php $i = 0;?>
<?php foreach($data as $photo):?>
	<?php 
	$id = $photo['Photo']['id'];
	$name = $photo['Photo']['name'];
	$desc = $photo['Photo']['desc'];
	$active = $photo['Photo']['active'];
	?>
	<div class="g-block g-fullwidth rounded bgwhite text-block">
		<?php echo $this->Form->hidden("$i.Photo.id",array('value'=>$id))?>
		<?php echo $this->Form->hidden("$i.Photo.album_id",array('value'=>$albumId))?>
		<table>
			<tr>
				<td>Foto</td>
				<td><?php echo $this->DataFotos->show(array(
					'model' => 'Photo',
					'file' => $photo['Photo']['file'],
					'id' => $photo['Photo']['id'],
					'size' => 'display'
				));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Form->label("$i.Photo.name","Título"); ?></td>
				<td><?php echo $this->Form->input("$i.Photo.name", array("value"=>$name,"label" => false,'class'=>'g-twowide')); ?></td>
			</tr>
			<tr>
				<td><?php echo $this->Form->label("$i.Photo.desc","Descrição"); ?></td>
				<td><?php echo $this->Form->input("$i.Photo.desc", array("value"=>$desc,"label" => false,'class'=>'g-twowide')); ?></td>
			</tr>
			<tr>
				<td><?php echo $this->Form->label("$i.Photo.active","Ativo"); ?></td>
				<?php if($active == 1):?>
				<td><?php echo $this->Form->input("$i.Photo.active", array("checked","label" => false,'class'=>'g-twowide')); ?></td>
				<?php else:?>
				<td><?php echo $this->Form->input("$i.Photo.active", array("label" => false,'class'=>'g-twowide')); ?></td>
				<?php endif;?>
			</tr>
		</table>
	</div>
	<?php $i++; ?>
<?php endforeach;?>
<?php echo $this->Form->submit("SALVAR EDIÇÕES",array('class'=>'send-form g-block g-fullwidth text-block'))?>
<?php echo $this->Form->end();?>