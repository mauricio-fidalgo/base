<?php
	$albumId = $album['Album']['id'];
	$trail = array(
		'Álbuns' => array(
			'controller' => 'Albums',
			'action' => 'index',
			'user' => true
		),
		'Álbum - '.$album['Album']['name'] => array(
			'controller' => 'Albums',
			'action' => 'view',
			'user' => true,
			$albumId
		),
		'Apagar Fotos'
	);
	echo $this->General->showTrail($trail);
?>
<div class="g-block g-fullwidth bgwhite title rounded">
	<h1><?php echo $album['Album']['name']?></h1>
</div>
<?php if($album['Album']['subtitle'] != ""): ?>
<div class="g-block g-fullwidth bgwhite title rounded">
	<h2><?php echo $album['Album']['subtitle']?></h2>
</div>
<?php endif; ?>
<?php if($album['Album']['desc'] != ""): ?>
<div class="g-block g-fullwidth bgwhite text-block rounded">
	<p><?php echo $album['Album']['desc']?></p>
</div>
<?php endif; ?>
<?php 
	echo $this->Form->create("Selector");
	$links = array(
		"Adicionar Fotos" => array("user" => true,
			"action"=>'add',
			$album['Album']['id']),
		"Editar Fotos" => array("user" => true,
			'action' => 'multiple',
			$album['Album']['id']),
		"Voltar para o Álbum" => array("user"=>true,
			"controller"=>"Albums",
			"action"=>'view',
			$album['Album']['id']),
		"Voltar para a lista de álbuns" => array("user"=>true,
			"controller"=>"Albums",
			'action'=>'index'),
		);
	echo $this->General->navList($links,"Ações");
	unset($links);
?>
<?php echo $this->Form->submit("Apagar fotos selecionadas",array("class"=>"btn btn-danger"));?>
<ul class="thumbnails">
<?php if(empty($album['Photo'])):?>
	<p>Este álbum não contem imagens.</p>
<?php else:?>
<?php foreach($album['Photo'] as $foto):?>
	<li class="span3">
		<div class="thumbnail">
			<?php echo $this->DataFotos->show(array(
					'model' => 'Photo',
					'id' => $foto['id'],
					'file' => $foto['file'],
					'size' => 'display')
				);
			?>
			<div class="caption">
					<?php echo $this->DataFotos->prettyPhoto("ver",
						array('model' => 'Photo',
							'id' => $foto['id'],
							'file' => $foto['file']),
						array('escape' => false,
							 'rel' => 'prettyPhoto[gal='.$album['Album']['name'].']',
							 'class' => 'btn')
						);
					?>
					<?php echo $this->Form->checkbox("Foto.marcado",array(
						'name'=>'data[Selector]['.$foto['id'].'][]',
						'hiddenField'=>false,
						'class' => 'pull-right'
						));
					?>
			</div>
		</div>
	</li>
<?php endforeach;?>
<?php endif;?>
</ul>
<?php echo $this->Form->end();?>