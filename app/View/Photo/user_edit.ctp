<div class="g-block g-fullwidth bgwhite block-padding">
	<?php echo $this->Form->create("Photo"); ?>
	<table>
		<tr>
			<td>Foto</td>
			<td><?php echo $this->DataFotos->show(array(
				'model' => 'Photo',
				'file' => $photo['Photo']['file'],
				'id' => $photo['Photo']['id'],
				'size' => 'display'
			));?></td>
		</tr>
		<tr>
			<td><?php echo $this->Form->label("name","Título"); ?></td>
			<td><?php echo $this->Form->input("name", array("label" => false,'class'=>'g-twowide')); ?></td>
		</tr>
		<tr>
			<td><?php echo $this->Form->label("desc","Descrição"); ?></td>
			<td><?php echo $this->Form->input("desc", array("label" => false,'class'=>'g-twowide')); ?></td>
		</tr>
		<tr>
			<td><?php echo $this->Form->label("active","Ativo"); ?></td>
			<td><?php echo $this->Form->input("active", array("label" => false,'class'=>'g-twowide')); ?></td>
		</tr>
	</table>
	<?php echo $this->Form->submit("Editar", array('class'=>'send-form'));?>
	<?php echo $this->Form->end(); ?>
	<div class="bgwhite g-block g-fullwidth rounded block-padding normalize-links">
	    <ul class="nav-devcloud inline-menu normalize-links">
	        <li class="nav-devcloud-actions"><?php echo $this->Html->link("Voltar para o album"
	        , array("controller" => "albums",
					"action" => "view",
					$photo['Photo']['album_id'])); ?></li>
	    </ul>
	</div>
</div>