<?php
App::uses('HtmlHelper','View/Helper');
class DataFotosHelper extends HtmlHelper{
	public $helpers = array('Html');
	/**
	 * getUrl
	 * Lê a variável values e retorna a URL da imagem desejada
	 * @param $values
	 * requer o nome da model,
	 * o nome do arquivo,
	 * e o id
	 * opicionalmente o tamanho do thumbnail
	 */
	public function getUrl($values = array(), $assetUrl = true){
		if(empty($values['model']) 
		|| empty($values['file']) 
		|| empty($values['id'])){
			return false;
		}
		if(!empty($values['size'])){
			$values['file'] = $values['size']."_".$values['file'];
		}
		$values['model'] = strtolower($values['model']);
		if($assetUrl)
			$path = $this->assetUrl("files/"
									. $values['model']
									. "/file/" . $values['id']
									. "/" . $values['file']);
		else $path = "/files/".$values['model']
					."/file/".$values['id']
					."/".$values['file'];
		return $path;
	}
	public function show($values = array(),$options = array()){
		if(!$this->getUrl($values)){
			return false;
		}
		$path = $this->getUrl($values);
		$image = sprintf($this->_tags['image'], $path, $this->_parseAttributes($options, null, '', ' '));
		return $image;
	}
	/**
	 * prettyPhoto
	 * Cria um link prettyphoto para uma DataPhoto
	 */
	public function prettyPhoto($text = array(),$to = array(),$options = array()){
		if(is_array($text))
			$text = $this->getUrl($text);
		$to = $this->getUrl($to,false);
		if(!$text || !$to) return false;
		if(!array_key_exists('escape', $options))
			$options['escape'] = false;
		if(is_array($text))
			$text = sprintf($this->_tags['image'],
				$text, $this->_parseAttributes(
					$options, null, '', ' '));
		return $this->link($text, $to, $options);
	}
} 
