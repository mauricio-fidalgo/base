<?php
class AllControllersHelper extends AppHelper
{
    public $helpers = array("Form");
    /**
     * 
     */
    public function toForm(array $controllers, $model = "Group", $show = true){
        $return = "<ul class='unstyled'>";
        foreach($controllers as $controller => $actions){
               $return .= "<li> <h2>" . $controller . "</h2>"
               . "<ul class='unstyled'>";
            foreach($actions as $action){
                $return .= "<li>"
                    . $this->Form->label($model . ".rules."
                                       . $controller . "." 
                                       . $action, $action, array("style" => "display:inline"))
                    . " " . $this->Form->checkbox($model . ".rules."
                                       . $controller . "." 
                                       . $action, array("style" => "float:left;display:block"))
                    . "</li>";
            }
            $return .= "</ul></li>";
        }
        $return .= "</ul>";
		if($show)
        	echo $return;
		else {
			return true;
		}
    }
    public function toFormEdit(array $rules, $model = "Group", $show = true){
        $return =  "<ul class='unstyled'>";
        foreach($rules as $controller => $actions){
            $return .= "<li> <h2>" . $controller . "</h2>"
            	. "<ul class='unstyled'>";   
            foreach($actions as $action => $value){
                $return .= "<li>"
               		. $this->Form->checkbox($model . ".rules."
                                       . $controller . "." 
                                       . $action, array(($value == 1)? "checked" : null))
                    . " " . $this->Form->label($model . ".rules."
                                       . $controller . "." 
                                       . $action, $action, array("style" => "display:inline"))
                    . "</li>";
            }
            $return .= "</ul></li>";
        }
        $return .= "</ul>";
        if($show)
        	echo $return;
		else {
			return true;
		}
    }
}