<?php
class GeneralHelper extends AppHelper{
	public $helpers = array("Html", "Paginator");
	public function __construct(View $view, $settings = array()) {
    	parent::__construct($view, $settings);
    	$this->settings = $settings;
		if(array_key_exists("Trail",$settings)){
			$this->trail = $settings["Trail"];
		}
    }
	/**
	 * showTrail
	 * esta função cria um caminho de links
	 */
	public function showTrail($trail = array()){
		if(empty($trail))
			return;
		$return = "<ul class=\"breadcrumb\">";
		foreach($trail as $key => $value){
			if(is_array($value)){
				$return .= "<li>";
				$return .= $this->Html->link($key, $value);
				$return .= " <span class=\"divider\">/</span>";
			} else {
				$return .= "<li class=\"active\">";
				$return .= $value;
			}	
			$return .= "</li>";
		}
		$return .= "</ul>";
		return $return;
	}
	public function navList($links = array(),$header = null){
		if(empty($links))
			return;
		$return = "";
		if($header !== null){
			$return .= "\t<h4>$header</h4>\n";
		}
		$return .= "<ul class=\"nav nav-pills\">\n";
		foreach($links as $name => $path){
			$return .= "\t<li>"
			. $this->Html->link($name,$path/*,array("class"=>"btn")*/)
			. "</li>\n";
		}
		$return .= "</ul>";
		return $return;
	}
	/**
	 * Função para preparar uma url com uma string variável
	 * para ajudar a função generate Table a substituir strings
	 */
	public function tableUrl($urlArray = Array(), $endStr){
		if(empty($urlArray))
			return;
		if(empty($endStr))
			return $this->Html->url($urlArray);
		return $this->Html->url($urlArray) . "/%".$endStr."%";
	}
	/**
	 * Gera uma tabela 
	 * $tableValues = {
	 *  Valores da tabela
	 * }
	 * $options = {
	 *  "Table" = {
	 *   Opções Html da tabela como class
	 *   id style e outros
	 *  }
	 *  "UseKeys" = {
     * 	 Se este array não estiver vazio as keys que não estiverem neste 
     *   array não seram exibidas
	 *  }
	 *  "Header" = {
	 *   Cabeçalho da tabela
	 *  }
	 *  "Foot" = {}
	 *  "Extras" = {
	 *    Adicionados a cada elemento no fim da linha, pode conter como
	 *    texto presente outras partes da tabela
	 *  }
	 * }
	 */
	public function generateTable($tableValues = array(), $options = array()){
		$table = "";
		$coluns = 0;
		$table .= "<table ";
		if(!empty($options["Table"])){
			foreach($options["Table"] as $key => $value){
				$table .= $key . "=";
				$table .= "\"".$value."\"";
				$table .= " ";
			}
		}
		$table .= " >";
		// Header
		if(!empty($options["Header"])){
			$table .= "<thead>";
			$table .= "<tr>";
			if(is_array($options["Header"])){
				foreach($options["Header"] as $header){
					$table .= "<th>";
					$table .= $header;
					$table .= "</th>";
				}
			}else
				$table .= $options["Header"];
			$table .= "</tr>";
			$table .= "</thead>";
		}
		// body
		$table .= "<tbody>";
		if(empty($options['UseKeys']))
			foreach($tableValues as $entry){
				$table .= "<tr>";
				foreach($entry as $value){
					$coluns++;
					$table .= "<td>";
					$table .= $value;
					$table .= "</td>";
				}
				$table .= "</tr>";
			}
		else{
			//sKeys => Keys para substituir
			$sKeys = array();
			if(!empty($options["Extras"])){
				reset($tableValues);
				$sKeys = array_keys($tableValues[key($tableValues)]);
				$sKeysSize = count($sKeys);
				for($i = 0; $i < $sKeysSize; $i++){
					$sKeys[$i] = "%".$sKeys[$i]."%"; 
				}
			}
			foreach($tableValues as $entry){
				$coluns++;
				$table .= "<tr>";
				foreach($options['UseKeys'] as $useKey){
					$table .= "<td>";
					if($useKey != "")
						$table .= $entry[$useKey];
					$table .= "</td>";
					$coluns++;
				}
				if(!empty($options["Extras"])){
					foreach($options["Extras"] as $extra){
						$coluns++;
						$table .= "<td>";
						if($value != "")
							$table .= str_replace($sKeys,array_values($entry),$extra);
						$table .= "</td>";
					}
				}
				$table .= "</tr>";
			}
		}
		$table .= "</tbody>";
		// foot
		
		if(!empty($options['Foot'])){
			$table .= "<tfoot>";
			$table .= "<tr>";
			$table .= "<td colspan=\"".$coluns."\">";
			foreach($options['Foot'] as $foot){
				if($foot == "Pagination"){
					$table .= '<div class="pagination pagination-centered">';
					$table .= "<ul>";
					$first = $this->Paginator->first("<<",array("title"=>"Primeira página"));
					$last = $this->Paginator->last(">>",array("title"=>"Ultima página"));
					if($first != ""){
						$table .= "<li>";
						$table .= $first;
						$table .= "</li>";
					}
					$table .= "<li>";
					$table .= $this->Paginator->prev("<",array("escape"=>false,"title"=>"Página anterior"));
					$table .= "</li>";
					$table .= "<li>";
					$table .= $this->Paginator->next(">",array("escape"=>false,"title"=>"Próxima página"));
					$table .= "</li>";
					if($last != ""){
						$table .= "<li>";
						$table .= $last;
						$table .= "</li>";
					}
					$table .= "</ul>";
					continue;
				}
			}
			$table .= "</td>";
			$table .= "</tr>";
			$table .= "</tfoot>";
		}
		$table .= "</table>";
		return $table;
	}
}
