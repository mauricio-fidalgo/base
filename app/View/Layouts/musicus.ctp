<?php
	
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $titleForLayout; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php echo $this->Html->css("musicus"); ?>
	</head>
	<body>
		<div class="holder" id="main">
			<div id="nav">
				<a class="logo" href="#">
					<?php echo $this->Html->image("logo.png"); ?>
				</a>
				<ul class="upper">
					<li class="active">
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Agenda</a>
					</li>
					<li>
						<a href="#">Cobertura</a>
					</li>
					<li>
						<a href="#">Colunas</a>
					</li>
					<li>
						<a href="#">Parceiros</a>
					</li>
					<li>
						<a href="#">Contato</a>
					</li>
				</ul>
			</div>
			<div class="user-call"><p class="upper">Faça <a href="#">login</a> ou <a href="#">cadastre-se</a></p></div>
			<div class="divisor nomarg-top"> </div>
			<div class="divisor"> </div>
			<div class="group3 articles over">
				<div class="col">
					<h3 class="upper">&rarr;<span>Emufrn</span></h3>
					<div class="divisor purple med nomarg"> </div>
					<p>A Ribeira é um lugar, muito lindo e magico, onde tudo acontece, onde voce pode caminhar livremente sem que nada aconteça de ruim na sua vida, porque Jah está ao seu lado, ele sempre vai te iluminar. E te abençoar.</p>
					<p><a href="#" class="upper b"><span class="arrow">&rarr;</span> Mais</a></p>
				</div>
				<div class="col">
					<h3 class="upper">&rarr;<span>Sindimusi</span></h3>
					<div class="divisor orange med nomarg"> </div>
				</div>
				<div class="col">
					<h3 class="upper">&rarr;<span>Espaço Cultural</span></h3>
					<div class="divisor yellow med nomarg"> </div>
				</div>
			</div>
			<div class="divisor"> </div>
			<div class="group4 articles over">
				<h3 class="upper clearboth marg">&rarr;<span>Cobertura</span></h3>
				<div class="col">
					<div class="humethumb">
						<a href="#">
							<span class="plus upper">Mais  &rarr;</span>
							<img src="bob.png"/>
						</a>
					</div>
				</div>
				<div class="col">
					<div class="humethumb">
						<a href="#">
							<span class="plus upper">Mais  &rarr;</span>
							<img src="bob.png"/>
						</a>
					</div>
				</div>
				<div class="col">
					<div class="humethumb">
						<a href="#">
							<span class="plus upper">Mais  &rarr;</span>
							<img src="bob.png"/>
						</a>
					</div>
				</div>
				<div class="col">
					<div class="humethumb">
						<a href="#">
							<span class="plus upper">Mais  &rarr;</span>
							<img src="bob.png"/>
						</a>
					</div>
				</div>
			</div>
			<div class="divisor"></div>
			<div id="bottom">
				<div class="bottom-left">
					<h3 class="upper"><span class="arrow">&rarr;</span> Blog Updates</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis nulla id orci malesuada porta posuere quis massa. Nunc vitae purus non augue scelerisque ultricies vitae et velit. Sed vitae lectus id sem lobortis scelerisque. Praesent eget consequat libero. </p>
				</div>
				<div class="bottom-right"></div>
			</div>
		</div>
	</body>
</html>