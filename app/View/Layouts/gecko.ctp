<?php 
    $Html = $this->Html;
    $home = $Html->url(array(
    	"user" => true,
        "controller" => "dashboard",
        "action" => "user_index"
    ));
    $albuns = $Html->url(array(
    	"user" => true,
        "controller" => "Albums",
        "action" => "user_index"
    ));
    $blogs = $Html->url(array(
    	"user" => true,
        "controller" => "Blogs",
        "action" => "user_index"
    ));
    $pages = $Html->url(array(
    	"user" => true,
        "controller" => "MenuPages",
        "action" => "index"
    ));
    $configurations = $Html->url(array(
    	"user" => true,
        "controller" => "Configuration",
        "action" => "user_index"
    ));
	$manageUsers = $Html->url(array(
		"user" => true,
		"controller" => "Users",
		"action" => "index"
	));
	$manageGroups = $Html->url(array(
		"user" => true,
		"controller" => "Groups",
		"action" => "index"
	));
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $titleForLayout; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php 
			if(isset($prettyPhoto))
				if($prettyPhoto == true)
					echo $this->Html->css("prettyPhoto");
			echo $this->Html->css(array("bootstrap.min","main"));
			echo $this->Html->script("jquery.js");
		?>
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<ul class="nav">
						<li><?php echo $Html->link("Voltar para o site","/"); ?></li>
						<li><?php echo $Html->link("Logout",array('user'=>false,'controller' => 'Users', 'action' => 'logout'));?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container gecko-main">
			<div class="row">
				<div class="span3">
					<div>
						<h3>Menu</h3>
					</div>
					<ul class="nav nav-tabs nav-stacked">
						<li>
	                        <a href="<?php echo $home;?>">
	                            Dashboard
	                        </a>
	                    </li>
	                    <li>
	                        <a href="<?php echo $pages;?>">
	                            Páginas
	                        </a>
	                    </li>
	                    <li>
	                        <a href="<?php echo $albuns?>">
	                            Fotos
	                        </a>
	                    </li>
	                    <li>
	                        <a href="<?php echo $blogs;?>">
	                            Blogs
	                        </a>
	                    </li>
	                    <li>
	                    	<a href="<?php echo $manageUsers; ?>">
	                    		Usuários
	                    	</a>
	                    </li>
	                    <li class="dropdown-submenu">
	                    	<a href="<?php echo $configurations;?>">
	                    		Configurações
	                    	</a>
	                    	<ul class="dropdown-menu">
	                    		<li><a href="<?php echo $manageGroups; ?>">Gerenciar Grupos</a></li>
	                    		<li><a href="<?php echo "#"?>">Informações do Site</a></li>
	                    	</ul>
	                    </li>
					</ul>
				</div>
				<div class="span9">
					<?php echo $this->Session->flash('flash',array('element'=>'flash_custom'))?>
					<?php echo $this->fetch('content'); ?>
				</div>
			</div>
		</div>
		<?php
		      echo $this->Html->script("bootstrap.min");
			  echo $this->Html->script("ckeditor/ckeditor.js");
			  echo $this->Html->script("base");
			  if(isset($prettyPhoto))
			  	if($prettyPhoto == true){
					echo $this->Html->script("jquery.prettyPhoto");
					echo "<script>
							$(document).ready(function(){
								$(\"a[rel^='prettyPhoto']\").prettyPhoto({social_tools:false});
							})
						</script>";
				}
		?>
	</body>
</html>