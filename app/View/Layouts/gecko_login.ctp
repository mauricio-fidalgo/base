<!DOCTYPE html>
<html>
	<head>
		<title>Teste</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php echo $this->Html->css(array("bootstrap.min","main"));?>
	</head>
	<body>
		<div class="container gecko-main">
			<?php echo $this->fetch('content'); ?>
		</div>
		<?php echo $this->Html->script("jquery.js");
		      echo $this->Html->script("bootstrap.min");
		?>
	</body>
</html>