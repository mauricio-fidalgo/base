<?php
	$trail = array(
		'Blogs'
	);
	echo $this->General->showTrail($trail);
?>
<div>
	<h1>Blogs</h1>
</div>
<?php echo $this->Form->create("Blog");?>
<table class="table table-bordered">
	<thead>
		<tr>
	        <th></th>
	        <th><?php echo $this->Paginator->sort("name","Título");?></th>
	        <th><?php echo $this->Paginator->sort("subtitle","Subtítulo");?></th>
	        <th><?php echo $this->Paginator->sort("created","Criado");?></th>
	        <th><?php echo $this->Paginator->sort("modified","Modificado");?></th>
	        <th>Ações</th>
    	</tr>
	</thead>
	<tbody>
    <?php foreach($data as $blog):?>
	    <tr>
	        <td><?php echo $this->Form->checkbox("Blog.checked.".$blog['Blog']['id'])?></td>
	        <td><?php echo $blog['Blog']['name']?></td>
	        <td><?php echo $blog['Blog']['subtitle']?></td>
	        <td><?php echo $blog['Blog']['created']?></td>
	        <td><?php echo $blog['Blog']['modified']?></td>
	        <td class="dropdown">
	        	<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
	        		Ações <b class="caret"></b>
				</a>
	        	<ul class="dropdown-menu pull-right">
		        	<li title="Ver">
						<?php echo $this->Html->link("Ver",
							array('action' => 'user_view',
								$blog['Blog']['id']),
							array('escape' => false));?>
					</li>
					<li title="Editar">
						<?php echo $this->Html->link("Editar",
							array('action' => 'user_edit',
								$blog['Blog']['id']),
							array('escape' => false));?>
					</li>
					<li title="Apagar">
						<?php if ($blog['Blog']['erasable'])
							echo $this->Form->postLink("Apagar",
							array('action' => 'user_delete',
								$blog['Blog']['id']),
							array('escape' => false),
								"Deseja realmente "
		                  	  . "apagar este Blog "
		                      . "juntamente com "
		                      . "suas postagens?");?>
		           	</li>
	        	</ul>	            
	    	</td>
	    </tr>
	    <?php endforeach;?>
    </tbody>
    <tfoot>
		<tr>
			<td colspan="30">
				<div class="pagination pagination-centered">	
					<ul>
						<?php 
							$first = $this->Paginator->first("<<",array("title"=>"Primeira página"));
							$last = $this->Paginator->last(">>",array("title"=>"Ultima página"));
						?>
						<?php if($first != "")echo "<li>".$first."</li>";else echo "<li><span>&#60;&#60;</span></li>"; ?>
						<li><?php echo $this->Paginator->prev("<",array("escape"=>false,"title"=>"Página anterior"));?></li>
						<?php echo $this->Paginator->numbers(array("tag"=>"li","separator"=>false)); ?>
						<li><?php echo $this->Paginator->next(">",array("escape"=>false,"title"=>"Próxima página"));?></li>
						<?php if($last != "")echo "<li>".$last."</li>";else echo "<li><span>&#62;&#62;</span></li>" ?>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<div>
	<?php echo $this->Html->link("Criar Novo Blog", array('user'=> true,'controller' =>'Blogs', 'action'=>'add'), array('class'=>'btn btn-primary'));?>
</div>
<?php echo $this->Form->end();?>