<div class="title g-block g-fullwidth rounded bgwhite">
    <h1>Editar Blog</h1>
</div>
<?php echo $this->Form->create("Blog"); ?>
<div class="g-block g-fullwidth bgwhite rounded block-padding">
    <table>
        <tr>
            <td class="top-aligned-text">
                <?php echo $this->Form->label("name","Título")?>
            </td>
            <td>
                <?php echo $this->Form->input("name",array("label" => false,'class'=>'g-block g-twowide')); ?>
            </td>
        </tr>
        <tr>
            <td class="top-aligned-text">
                <?php echo $this->Form->label("subtitle","Subtítulo")?>
            </td>
            <td>
                <?php echo $this->Form->input("subtitle",array("label" => false,'class'=>'g-block g-twowide')); ?>
            </td>
        </tr>
        <tr>
            <td class="top-aligned-text">
                <?php echo $this->Form->label("desc","Descrição"); ?>
            </td>
            <td>
                <?php echo $this->Form->input("desc",array("label" => false,'class'=>'g-block g-twowide')); ?>
            </td>
        </tr>
        <tr>
        	<td class="top-aligned-text"><?php echo $this->Form->label("active","Ativo")?></td>
        	<td>
        		 <?php echo $this->Form->input("active",array("label"=>false)); ?>
        	</td>
        </tr>
    </table>
    <?php echo $this->Form->submit("Enviar", array("class"=>"send-form")); ?>
    <?php echo $this->Form->end(); ?>
</div>
<div class="bgwhite g-block g-fullwidth rounded block-padding normalize-links">
    <ul class="nav-devcloud inline-menu">
        <li class="nav-devcloud-actions">
            <?php echo $this->Html->link("<< Voltar para a lista de blogs",array(
                "user" => true,
                "controller" => "Blogs",
                "action" => "index"
            ))?>
        </li>
    </ul>
</div>