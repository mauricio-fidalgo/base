<?php if($blog["Blog"]["name"] != ""):?>
<div>
    <h1><?php echo $blog["Blog"]["name"]; ?></h1>
</div>
<?php endif;?>
<?php if($blog["Blog"]["subtitle"] != ""):?>
<div>
	<h2><?php echo $blog["Blog"]["subtitle"]; ?></h2>
</div>
<?php endif;?>
<?php if($blog["Blog"]["desc"] != ""):?>
<div>
	<p><?php echo $blog["Blog"]["desc"]; ?></p>
</div>
<?php endif ?>
<h3>Lista de Postagens</h3>
<table class="table table-bordered">
	<thead>
		<tr class="table-devcloud-header">
	        <th></th>
	        <th><?php echo $this->Paginator->sort("name","Título");?></th>
	        <th><?php echo $this->Paginator->sort("subtitle","Subtítulo"); ?></th>
	        <th><?php echo $this->Paginator->sort("call","Chamada");?>
	        <th><?php echo $this->Paginator->sort("created","Criado em");?></th>
	        <th><?php echo $this->Paginator->sort("modified","Modificado em");?></th>
	        <th>Ações</th>
    	</tr>
	</thead>
	<tbody>
    <?php foreach($paginate as $post):?>
        <tr>
            <td><?php echo $post["BlogPost"]["id"]?></td>
            <td><?php echo $post["BlogPost"]["name"]?></td>
            <td><?php echo $post["BlogPost"]["subtitle"]?></td>
            <td><?php echo $post["BlogPost"]["call"]?></td>
            <td><?php echo $post["BlogPost"]["created"]?></td>
            <td><?php echo $post["BlogPost"]["modified"]?></td>
            <td class="dropdown">
            	<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">Ações <b class="caret"></b></a>
	    		<ul class="dropdown-menu gecko-dropToLeft">
	    		<li>
					<?php echo $this->Html->link("Ver",
						array('action' => 'user_view',
							"controller" => "BlogPosts",
							$post["BlogPost"]['id']),
						array('escape' => false));?></li>
					<li>
					<?php echo $this->Html->link("Editar",
						array('action' => 'user_edit',
							"controller" => "BlogPosts",
							$post["BlogPost"]['id']),
						array('escape' => false));?></li>
					<li>
						<?php
							echo $this->Form->postLink("Apagar",
							array('action' => 'user_delete',
								"controller" => "BlogPosts",
								$post["BlogPost"]['id']),
							array('escape' => false),
								"Deseja realmente apagar este Post?");?></li>
		    	</td>
        </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
    	<tr class="nav-devcloud">
    		<td colspan="30">
    			<div>	
					<span><?php echo $this->Paginator->first("<<Primeira Página",array("escape"=>false,"title"=>"Primeira página"));?></span>
					<span><?php echo $this->Paginator->prev("Página Anterior",array("escape"=>false,"title"=>"Página anterior"));?></span>
					<span><?php echo $this->Paginator->numbers(); ?></span>
					<span><?php echo $this->Paginator->next("Próxima Página",array("escape"=>false,"title"=>"Próxima página"));?></span>
					<span><?php echo $this->Paginator->last("Ultima Página",array("escape"=>false,"title"=>"Ultima página"));?></span>
				</div>
			</td>
    	</tr>
    </tfoot>
</table>
<?php
	$nav = array(
		"Voltar para a lista de Blogs" => array(
			"action" => "index",
			"user" => true),
		"Adicionar novos Posts"=>array(
	       "controller" => "BlogPosts",
	       "action" => "add",
	       "user" => true,
	 	   $id)
	);
	echo $this->General->navList($nav);
?>
